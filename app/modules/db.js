const mysql = require('mysql');
const fs = require('fs');

const config = JSON.parse(fs.readFileSync(__dirname + '/../config/config.json'));

function connect(req, res, next) {
    const dbParams = {
        host: config.server.host,
        user: config.mysql.user,
        password: config.mysql.password,
        database: config.mysql.database
    };

    var connection = mysql.createConnection(dbParams);

    connection.connect();
    return connection;
}

function getThreadsCount(connection, category) {
    return new Promise((resolve, reject) => {
        let categoryQuery = (category === undefined) ? "" : " AND category = " + mysql.escape(category);
        let getThreadsCountQuery = "SELECT COUNT(*) AS count FROM threads WHERE is_deleted = 0" + categoryQuery;

        connection.query(getThreadsCountQuery, (err, results, fields) => {
            if (err) throw err;

            resolve(results[0].count);
        });
    });
}

function getThreads(connection, mode, page, category) {
    return new Promise((resolve, reject) => {
        let categoryQuery = (category === undefined) ? "" : "AND category = " + mysql.escape(category);

        // BUG
        // Query returns null for the last id when not specifying "threads.id"
        // MYSQL : "ambiguous term"

        // Default front page mode
        let getThreadsQuery = "SELECT * FROM threads WHERE is_deleted = 0 "+categoryQuery+" ORDER BY created DESC LIMIT ?,?";

        if (mode === "all") {
            // Already default
        } else if (mode === "best") {
            getThreadsQuery = "SELECT *, threads.id, \
                              COALESCE(SUM(vote),0) as rating\
                              FROM threads \
                              LEFT JOIN threads_votes ON threads.id = threads_votes.thread_id \
                              WHERE is_deleted = 0 \
                              " + categoryQuery + " \
                              GROUP BY threads.id \
                              ORDER BY rating DESC \
                              LIMIT ?,?";
        } else if (mode === "vote") {
            getThreadsQuery = "SELECT *, threads.id, \
                              COUNT(threads_votes.id) AS vote_count\
                              FROM threads \
                              LEFT JOIN threads_votes ON threads.id = threads_votes.thread_id \
                              WHERE is_deleted = 0 \
                              " + categoryQuery + " \
                              GROUP BY threads.id \
                              ORDER BY vote_count DESC \
                              LIMIT ?,?";
        }

        let interval = 20;
        let intervalLow = (page * interval);
        let intervalHigh = (page * interval) + interval;

        connection.query(getThreadsQuery, [intervalLow, intervalHigh], (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function findThreads(connection, page, searchTerms) {
    searchTerms = searchTerms || '';

    searchTerms = '%' + searchTerms + '%';

    return new Promise((resolve, reject) => {
        let intervalLow = (page * 20);
        let intervalHigh = (page * 20) + 20;

        // BUG
        // -> "threads.id AS tid"
        // cf. getThreads (the above function) bug comment

        let getThreadsQuery = "SELECT *, threads.id FROM threads WHERE title LIKE " + mysql.escape(searchTerms) + " ORDER BY last_modified ASC LIMIT ?,?";
        connection.query(getThreadsQuery, [intervalLow, intervalHigh], (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });

    });
}

function getUserId(connection, pseudo) {
    return new Promise((resolve, reject) => {
        let userIdQuery = "SELECT id FROM users WHERE pseudo = ?";
        connection.query(userIdQuery, pseudo, (err, results, fields) => {
            if (err) throw err;

            resolve(results[0].id);
        });
    });
}

function getUserToken(connection, username) {
    return new Promise((resolve, reject) => {
        let getTokenQuery = "SELECT token FROM users WHERE pseudo = ?"
        connection.query(getTokenQuery, username, (err, results, fields) => {
            if (err) throw err;

            if (results[0] === undefined) {
                resolve(null);
            } else {
                resolve(results[0].token);
            }
        });
    });
}

function getUserInfo(connection, pseudo) {
    return new Promise((resolve, reject) => {
        let userInfoQuery = "SELECT * FROM users WHERE pseudo = ?";
        connection.query(userInfoQuery, pseudo, (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function getInfo(connection, table, id) {
    return new Promise((resolve, reject) => {
        let getInfoQuery = "SELECT * FROM " + table + " WHERE id = ?";
        connection.query(getInfoQuery, id, (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function getRoleInfo(connection, id) {
    return new Promise((resolve, reject) => {
        let roleInfoQuery = "SELECT * FROM roles WHERE id = ?";
        connection.query(roleInfoQuery, id, (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function getLastThreadId(connection) {
    return new Promise((resolve, reject) => {
        let lastThreadIdQuery = "SELECT id FROM threads ORDER BY id DESC LIMIT 0,1";
        connection.query(lastThreadIdQuery, (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function getThreadMessages(connection, id) {
    return new Promise((resolve, reject) => {
        let threadMessagesQuery = "SELECT * FROM messages WHERE thread_id = ?";
        connection.query(threadMessagesQuery, id, (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function insertInto(connection, table, value) {
    return new Promise((resolve, reject) => {
        let insertQuery = "INSERT INTO " + table + " SET ?";
        connection.query(insertQuery, value, (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function pushToken(connection, token, user) {
    let tokenValue = {
        token: token
    };

    return new Promise((resolve, reject) => {
        let insertQuery = "UPDATE users SET ? WHERE pseudo = ?";
        connection.query(insertQuery, [tokenValue, user], (err, results, fields) => {
            if (err) throw err;

            resolve(err, results[0]);
        });
    });
}

function fetchTags(connection, socket) {
    let fetchTagQuery = "SELECT tags FROM threads"
    connection.query(fetchTagQuery, (err, results, fields) => {
        let tagList = [];
        let tagArray;

        results.forEach((subArray) => {
            tagArray = subArray.tags.split(',');
            tagArray.forEach((el) => {
                tagList[tagList.length] = el;
            });
        });

        socket.emit('tagList', tagList);
    });
}

function getClass(connection, user_id) {
    return new Promise((resolve, reject) => {
        let getClassQuery = "SELECT * FROM classes WHERE id = (SELECT class_id FROM users WHERE id = ?)";
        connection.query(getClassQuery, user_id, (err, results, fields) => {
            if (err) throw err;
            console.log(user_id);

            resolve(results[0]);
        });
    });
}

function getClasses(connection) {
    return new Promise((resolve, reject) => {
        let getClassesQuery = "SELECT * FROM classes";
        connection.query(getClassesQuery, (err, results, fields) => {
            if (err) throw err;

            console.log(results);

            resolve(results);
        });
    });
}

function updateThread(connection, values, threadId) {
    return new Promise((resolve, reject) => {
        let updateThreadQuery = "UPDATE threads SET ? WHERE id = ?";
        connection.query(updateThreadQuery, [values, threadId], (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function updateMessage(connection, values, messageId) {
    return new Promise((resolve, reject) => {
        let updateMessageQuery = "UPDATE messages SET ? WHERE id = ?";
        connection.query(updateMessageQuery, [values, messageId], (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function checkFavorite(connection, threadId, userId) {
    return new Promise((resolve, reject) => {
        let checkFavoriteQuery = "SELECT is_active FROM favorited WHERE thread_id = ? AND user_id = ?";
        connection.query(checkFavoriteQuery, [threadId, userId], (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function updateFavorite(connection, threadId, userId) {
    return new Promise((resolve, reject) => {
        let updateFavoriteQuery = "UPDATE favorited SET is_active = CASE WHEN is_active = 1 THEN 0 ELSE 1 END WHERE (thread_id = ? AND user_id = ?)";
        connection.query(updateFavoriteQuery, [threadId, userId], (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function checkSub(connection, threadId, userId) {
    return new Promise((resolve, reject) => {
        let checkSubQuery = "SELECT is_active FROM subscribed WHERE thread_id = ? AND user_id = ?";
        connection.query(checkSubQuery, [threadId, userId], (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function updateSub(connection, threadId, userId) {
    return new Promise((resolve, reject) => {
        let updateSubQuery = "UPDATE subscribed SET is_active = CASE WHEN is_active = 1 THEN 0 ELSE 1 END WHERE (thread_id = ? AND user_id = ?)";
        connection.query(updateSubQuery, [threadId, userId], (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function toggleVote(connection, table, threadId, userId, voteMode, messageId) {
    return new Promise((resolve, reject) => {
        let additionalWhere = (table === "messages_votes") ? "AND message_id = ?" : "";
        let voteValue = (voteMode === "upvote") ? 1 : -1;

        let values = (messageId === undefined) ? [threadId, userId] : [threadId, userId, messageId];

        let toggleVoteQuery = "UPDATE " + table + " SET vote = CASE WHEN vote = " + voteValue + " THEN 0 ELSE " + voteValue + " END WHERE (thread_id = ? AND user_id = ? " + additionalWhere + ")";
        connection.query(toggleVoteQuery, values, (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function hasUserVotedThread(connection, threadId, userId) {
    return new Promise((resolve, reject) => {
        let hasVotedQuery = "SELECT vote FROM threads_votes WHERE thread_id = ? AND user_id = ?";
        connection.query(hasVotedQuery, [threadId, userId], (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function hasUserVotedMessage(connection, messageId, threadId, userId) {
    return new Promise((resolve, reject) => {
        let hasVotedQuery = "SELECT vote FROM messages_votes WHERE message_id = ? AND thread_id = ? AND user_id = ?";
        connection.query(hasVotedQuery, [messageId, threadId, userId], (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function getThreadScore(connection, threadId) {
    return new Promise((resolve, reject) => {
        let getScoreQuery = "SELECT SUM(vote) AS score FROM threads_votes WHERE thread_id = ?";
        connection.query(getScoreQuery, threadId, (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function getMessageVotes(connection, messageId, threadId) {
    return new Promise((resolve, reject) => {
        let getVotesQuery = "SELECT * FROM messages_votes WHERE message_id = ? AND thread_id = ?";
        connection.query(getVotesQuery, [messageId, threadId], (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function getThreadScoreData(connection, threadId) {
    return new Promise((resolve, reject) => {
        let threadScoreDataQuery = "SELECT * FROM threads_votes WHERE thread_id = ? ";
        connection.query(threadScoreDataQuery, threadId, (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function updateScore(connection, username) {
    return new Promise((resolve, reject) => {
        let updateScoreQuery = "UPDATE users SET score = score+20 WHERE pseudo = ?";
        connection.query(updateScoreQuery, username, (err, results, fields) => {
            if (err) throw err;

            resolve(results[0]);
        });
    });
}

function getAnswerCount(connection, threadId) {
    return new Promise((resolve, reject) => {
        let getAnswerCountQuery = "SELECT COUNT(id) AS answerCount FROM messages WHERE thread_id = ?";
        connection.query(getAnswerCountQuery, threadId, (err, results, fields) => {
            if (err) throw err;

            resolve(results[0].answerCount);
        });
    });
}

function getVoteCount(connection, threadId) {
    return new Promise((resolve, reject) => {
        let getVoteCountQuery = "SELECT COUNT(id) AS voteCount FROM messages_votes WHERE thread_id = ? AND vote <> 0";
        connection.query(getVoteCountQuery, threadId, (err, results, fields) => {
            if (err) throw err;

            resolve(results[0].voteCount);
        });
    });
}

function fetchFavs(connection, userId) {
    return new Promise((resolve, reject) => {
        let fetchFavQuery = "SELECT * FROM favorited WHERE user_id = ? AND is_active = 1";
        connection.query(fetchFavQuery, userId, (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function fetchSubs(connection, userId) {
    return new Promise((resolve, reject) => {
        let fetchSubsQuery = "SELECT * FROM subscribed WHERE user_id = ? AND is_active = 1";
        connection.query(fetchSubsQuery, userId, (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function addNotification(connection, socket, type, data, lastId) {
    console.log("type", type);
    if (type === "mention") {
        getUserId(connection, data.memberName).then((userId) => {
            let values = {
                user_id: userId,
                content: data.username + " vous a mentionné dans \"" + data.threadTitle + "\"",
                type,
                href: "/thread/" + data.threadId + "/0#" + data.postId
            }

            insertInto(connection, "notifications", values).then((results) => {

                let notificationData = data;
                notificationData.userId = userId;
                notificationData.content = values.content;
                notificationData.type = values.type;
                notificationData.href = values.href;
                notificationData.lastNotificationId = results.insertId;

                socket.broadcast.emit('newNotificationCreated', notificationData);
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF insertInto PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getUserId PROMISE
    } else if (type === "subscribe") {
        let values = {
            user_id: data.subscriberId,
            content: data.username + " a ajouté un commentaire dans \"" + data.threadTitle + "\"",
            type,
            href: "/thread/" + data.threadId + "/0"
        }

        insertInto(connection, "notifications", values).then((results) => {

            let notificationData = data;
            notificationData.userId = values.user_id;
            notificationData.content = values.content;
            notificationData.type = values.type;
            notificationData.href = values.href;
            notificationData.lastNotificationId = results.insertId;

            socket.broadcast.emit('newNotificationCreated', notificationData);
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF insertInto PROMISE
    } else if (type === "newAnswer") {
        let values = {
            user_id: data.author_id,
            content: data.username + " a ajouté un commentaire dans votre fil \"" + data.threadTitle + "\"",
            type,
            href: "/thread/" + data.threadId + "/0#" + data.postId
        }

        insertInto(connection, "notifications", values).then((results) => {
            let notificationData = data;

            notificationData.userId = values.user_id;
            notificationData.content = values.content;
            notificationData.type = values.type;
            notificationData.href = values.href;
            notificationData.lastNotificationId = results.insertId;

            socket.broadcast.emit('newNotificationCreated', notificationData);
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF insertInto PROMISE
    } else if (type === "validateAnswer") {
        let values = {
            user_id: data.chosenId,
            content: data.username + " a selectionné votre réponse dans le fil \"" + data.threadTitle + "\"",
            type,
            href: "/thread/" + data.threadId + "/0#" + data.postId
        }

        insertInto(connection, "notifications", values).then((results) => {

            let notificationData = data;
            notificationData.userId = values.user_id;
            notificationData.content = values.content;
            notificationData.type = values.type;
            notificationData.href = values.href;
            notificationData.lastNotificationId = results.insertId;

            socket.broadcast.emit('newNotificationCreated', notificationData);
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF insertInto PROMISE
    } else if (type === "messageRoleUnlocked") {
        let values = {
            user_id: data.user_id,
            content: "Vous avez débloqué le role \"" + data.role_name + "\" !",
            type,
            href: "/profile/" + data.username + "#tab-3"
        }

        insertInto(connection, "notifications", values).then((results) => {
            let notificationData = data;
            notificationData.userId = values.user_id;
            notificationData.content = values.content;
            notificationData.type = values.type;
            notificationData.href = values.href;
            notificationData.lastNotificationId = results.insertId;

            socket.emit('newNotificationCreated', notificationData);
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF insertInto PROMISE
    } else if (type === "scoreRoleUnlocked") {
        let values = {
            user_id: data.user_id,
            content: "Vous avez débloqué le role \"" + data.role_name + "\" !",
            type,
            href: "/profile/" + data.username + "#tab-3"
        }

        insertInto(connection, "notifications", values).then((results) => {
            let notificationData = data;
            notificationData.userId = values.user_id;
            notificationData.content = values.content;
            notificationData.type = values.type;
            notificationData.href = values.href;
            notificationData.lastNotificationId = results.insertId;

            socket.broadcast.emit('newNotificationCreated', notificationData);
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF insertInto PROMISE
    }
}

function getUserNotifs(connection, userId) {
    return new Promise((resolve, reject) => {
        let fetchNotificationsQuery = "SELECT * FROM notifications WHERE user_id = ? AND is_read = 0";
        connection.query(fetchNotificationsQuery, userId, (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function markNotificationAsRead(connection, notificationId) {
    return new Promise((resolve, reject) => {
        let markNotificationAsReadQuery = "UPDATE notifications SET is_read = 1 WHERE id = ?";
        connection.query(markNotificationAsReadQuery, notificationId, (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function getThreadSubscribers(connection, threadId) {
    return new Promise((resolve, reject) => {
        let getLastIdQuery = "SELECT * FROM subscribed WHERE thread_id = ? AND is_active = 1";
        connection.query(getLastIdQuery, threadId, (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function getUserMessageCount(connection, uid) {
    return new Promise((resolve, reject) => {
        let getUserMessageCountQuery = "SELECT COUNT(id) AS messageCount FROM messages WHERE author_id = ?";
        connection.query(getUserMessageCountQuery, uid, (err, results, fields) => {
            if (err) throw err;

            resolve(results[0].messageCount);
        });
    });
}

function getUserRolesInfo(connection, uid) {
    return new Promise((resolve, reject) => {
        let getUserRolesInfoQuery = "SELECT * FROM users_roles WHERE user_id = ? ORDER BY id DESC";
        connection.query(getUserRolesInfoQuery, uid, (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function getRolesData(connection) {
    return new Promise((resolve, reject) => {
        let getRolesDataQuery = "SELECT * FROM roles";
        connection.query(getRolesDataQuery, (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function addNewUserRole(connection, values) {
    return new Promise((resolve, reject) => {
        let addNewUserRoleQuery = "INSERT INTO users_roles SET ?";
        connection.query(addNewUserRoleQuery, values, (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function checkRole(connection, user_id, role_id) {
    return new Promise((resolve, reject) => {
        let getUserRolesInfoQuery = "SELECT * FROM users_roles WHERE user_id = ? AND role_id = ?";
        connection.query(getUserRolesInfoQuery, [user_id, role_id], (err, results, fields) => {
            if (err) throw err;

            if (results.length > 0) {
                resolve(true);
            } else {
                resolve(false);
            }
        });
    });
}

function selectRole(connection, roleId, userId) {
    return new Promise((resolve, reject) => {
        let selectRoleQuery = "UPDATE users SET role_id = ? WHERE id = ?";
        connection.query(selectRoleQuery, [roleId, userId], (err, results, fields) => {
            if (err) throw err;

            resolve(results);
        });
    });
}

function getUserScore(connection, uid) {
    return new Promise((resolve, reject) => {
        let getUserMessageCountQuery = "SELECT score FROM users WHERE id = ?";
        connection.query(getUserMessageCountQuery, uid, (err, results, fields) => {
            if (err) throw err;

            resolve(results[0].score);
        });
    });
}

function getPseudoFromPostId(connection, postId) {
    return new Promise((resolve, reject) => {
        let getUserIdQuery = "SELECT author_id FROM messages WHERE id = ?";
        connection.query(getUserMessageCountQuery, postId, (err, results, fields) => {
            if (err) throw err;
            let getUserPseudoQuery = "SELECT pseudo FROM users WHERE id = ?";
            connection.query(getUserPseudoQuery, postId, (err, results, fields) => {
                if (err) throw err;
                resolve(results[0].pseudo);
            });
        });
    });
}

module.exports = {
    connect,
    getUserInfo,
    getUserId,
    getRoleInfo,
    getInfo,
    getThreadsCount,
    getThreads,
    findThreads,
    getUserToken,
    pushToken,
    getLastThreadId,
    getThreadMessages,
    insertInto,
    fetchTags,
    getClass,
    getClasses,
    updateThread,
    updateMessage,
    checkFavorite,
    updateFavorite,
    checkSub,
    updateSub,
    toggleVote,
    hasUserVotedThread,
    hasUserVotedMessage,
    getThreadScore,
    getMessageVotes,
    getThreadScoreData,
    updateScore,
    getAnswerCount,
    getVoteCount,
    fetchFavs,
    fetchSubs,
    addNotification,
    getUserNotifs,
    markNotificationAsRead,
    getThreadSubscribers,
    getUserMessageCount,
    getUserRolesInfo,
    getRolesData,
    addNewUserRole,
    checkRole,
    selectRole,
    getUserScore,
    getPseudoFromPostId
};
