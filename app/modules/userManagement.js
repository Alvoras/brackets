const express = require('express');
const app = express();

const fs = require('fs');
const crypto = require('crypto');
const bcrypt = require('bcryptjs');
const db = require('../modules/db');
const connection = db.connect();
const config = JSON.parse(fs.readFileSync(__dirname + '/../config/config.json'));
const mailAccount = JSON.parse(fs.readFileSync(__dirname + '/../config/mail.json'));

// AUTHENTICATE
const session = require('express-session');
const cookieParser = require('cookie-parser');

// MAIL
const nodemailer = require('nodemailer');

app.use(cookieParser());

app.use(session({
    secret: config.secrets.sessionSecret,
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: false, // when in production : true
        expires: false
    }
}));

function generatePwd(pwdLength) {
    let pool = "azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN123456789";
    let randNb;
    let pwd = '';
    let i = 0;

    for (i = 0; i < pwdLength; i++) {
        randNb = Math.floor((Math.random() * pool.length) + 1);
        pwd += pool.charAt(randNb);
    }

    return pwd;
}

function sendReportMail(data) {
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: mailAccount.modMail.credentials.user,
            pass: mailAccount.modMail.credentials.pwd
        }
    });

    let plainText = '\n\
                Le post ' + data.postId + ' du sujet \"' + data.threadTitle + '\" a été signalé : \n\
                \n\
                ' + data.postContent + '\
                \n\
                \t  * Utilisateur : ' + data.username + '\n';

    let htmlText = '<br>\
                Le post ' + data.postId + ' du sujet <b>\"' + data.threadTitle + '\"</b> a été signalé : <br>\
                <br>**********************************\
                ' + data.postContent + '\
                <br>**********************************\
                <ul>\
                    <li>Utilisateur : <b>' + data.username + '</b><br>\
                </ul>';

    let mailOptions = {
        from: '"Utilisateur Brackets" <moderation.brackets@gmail.com>', // sender address
        to: 'moderation.brackets@gmail.com', // list of receivers
        subject: 'Nouveau signalement', // Subject line
        text: plainText, // plain text body
        html: htmlText // html body
    };

    // send mail with defined transport object
    return transporter.sendMail(mailOptions);
}

function sendWelcomeMail(email, userData) {
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: mailAccount.welcomeMail.credentials.user,
            pass: mailAccount.welcomeMail.credentials.pwd
        }
    });

    let plainText = 'Bienvenue sur Brackets !\n\
                \n\
                Votre compte a été créé. Vous pouvez vous connecter avec les identifiants :\n\
                \n\
                \t  * Utilisateur : ' + userData.username + '\n\
                \t  * Mot de passe : ' + userData.password + '\n\
                \n\
                Ce mot de passe est temporaire, il vous sera demandé d\'en définir un nouveau lors de votre première connexion.\n\
                \n\
                Rendez-vous sur https://esgi-brackets.fr pour vous connecter !\n\
                \n\
                \t - L\'équipe Brackets';


    let htmlText = '<b>Bienvenue sur Brackets !</b><br>\
                <br>\
                Votre compte a été créé. Vous pouvez vous connecter avec les identifiants :<br>\
                <ul>\
                    <li>Utilisateur : <b>' + userData.username + '</b><br>\
                    <li>Mot de passe : <b>' + userData.password + '</b><br>\
                </ul>\
                Ce mot de passe est temporaire, il vous sera demandé d\'en définir un nouveau lors de votre première connexion.<br>\
                <br>\
                Rendez-vous sur https://esgi-brackets.fr pour vous connecter !<br>\
                <br>\
                   - L\'équipe Brackets';

    let mailOptions = {
        from: '"ESGI Brackets" <esgi.brackets@gmail.com>', // sender address
        to: email, // list of receivers
        subject: 'Compte ouvert !', // Subject line
        text: plainText, // plain text body
        html: htmlText // html body
    };

    // send mail with defined transport object
    return transporter.sendMail(mailOptions);
}

function sendNewPassword(userData) {
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: mailAccount.welcomeMail.credentials.user,
            pass: mailAccount.welcomeMail.credentials.pwd
        }
    });

    let plainText = 'Forum Brackets :\n\
                \n\
                Vos identifiants ont étés réinitialisés suite à votre demande. Nouveaux identifiants :\n\
                \n\
                \t  * Utilisateur : ' + userData.pseudo + '\n\
                \t  * Mot de passe : ' + userData.password + '\n\
                \n\
                Ce mot de passe est temporaire, il vous sera demandé d\'en définir un nouveau lors de votre première connexion.\n\
                \n\
                Rendez-vous sur https://esgi-brackets.fr pour vous connecter !\n\
                \n\
                Si vous n\'êtes pas à l\'origine de ces changements, n\'hésitez pas à contacter l\'équipe de modération via moderation.brackets@gmail.com.\n\
                \n\
                \t - L\'équipe Brackets';


    let htmlText = '<b>Forum Brackets :</b><br>\
                <br>\
                Vos identifiants ont étés réinitialisés suite à votre demande. Nouveaux identifiants :\n\
                <ul>\
                    <li>Utilisateur : <b>' + userData.pseudo + '</b><br>\
                    <li>Mot de passe : <b>' + userData.password + '</b><br>\
                </ul>\
                Ce mot de passe est temporaire, il vous sera demandé d\'en définir un nouveau lors de votre première connexion.<br>\
                <br>\
                Rendez-vous sur https://esgi-brackets.fr pour vous connecter !<br>\
                <br>\
                Si vous n\'êtes pas à l\'origine de ces changements, n\'hésitez pas à contacter l\'équipe de modération via moderation.brackets@gmail.com.\n\
                <br>\
                   - L\'équipe Brackets';

    let mailOptions = {
        from: '"ESGI Brackets" <esgi.brackets@gmail.com>', // sender address
        to: userData.email, // list of receivers
        subject: 'Nouveaux identifiants', // Subject line
        text: plainText, // plain text body
        html: htmlText // html body
    };

    // send mail with defined transport object
    return transporter.sendMail(mailOptions);
}

function resetId(connection, socket, userData) {
    connection.query("SELECT id FROM users WHERE email = ?", userData.email, (err, results, fields) => {
        if (results.length === 0){
            socket.emit('sendNewPasswordFailed');
            return;
        } else {
            connection.query("SELECT id FROM users WHERE pseudo = ?", userData.pseudo, (err, results, fields) => {
                if (results.length === 0) {
                    socket.emit('sendNewPasswordFailed');
                    return;
                } else {
                    let plainPassword = generatePwd(12);

                    let salt = bcrypt.genSaltSync(10);
                    let encryptedPwd = bcrypt.hashSync(plainPassword, salt);

                    userData.password = plainPassword;

                    // Send mail with temporary credentials
                    sendNewPassword(userData).then(() => {
                        // If the mail is correctly sent, add user to DB
                        let updatePwdQuery = "UPDATE users SET ? WHERE pseudo = ?";
                        connection.query(updatePwdQuery, [{password: encryptedPwd, first_time: 1}, userData.pseudo], (err, results, fields) => {
                            if (err) throw err;

                            socket.emit('sendNewPasswordSuccess');
                        }).catch((err) => setImmediate(() => {
                            throw err;
                        })); // END OF insertInto PROMISE
                    });
                }
            });
        }
    });
}

function generateToken(length) {
    const buffer = crypto.randomBytes(length);
    let token = buffer.toString('hex');

    return token;
}

function checkHmac(localHmac, dbHmac) {
    if (localHmac === dbHmac) {
        return true;
    } else {
        return false;
    }
}

function checkToken(localToken, dbToken) {
    if (localToken === dbToken) {
        return true;
    } else {
        return false;
    }
}

function getUsername(req) {
    if ((req.cookies["rememberMe"] !== undefined)) {
        return username = req.cookies["rememberMe"].split(':')[0];
    } else {
        req.session.error = "You can't access this page without being logged in";
        return 0;
    }
}

function authenticate(req, res, params) {
    if ((req.cookies["rememberMe"] !== '' && req.cookies["rememberMe"] !== undefined)) {
        console.log("COOKIE", req.cookies["rememberMe"]);
        let [username, token, hmac] = req.cookies["rememberMe"].split(':');
        let cookie = [];
        cookie["username"] = username;
        cookie["token"] = token;
        cookie["hmac"] = hmac;

        if (params.user !== undefined) {
            if (cookie["username"] !== params.user) {
                res.cookie("rememberMe", '');
                req.session.error = "Impossible d'accéder à cette page";
                return 0;
            }
        }

        let getUserTokenQuery = "SELECT token FROM users WHERE pseudo = ?";

        connection.query(getUserTokenQuery, cookie["username"], (err, results, fields) => {

            let userToken = results[0].token;
            if (userToken !== cookie["token"]) {
                res.cookie("rememberMe", '');
                req.session.error = "Impossible d'accéder à cette page";
                return 0;
            }

            // Generate hmac signature from current info
            let hmac = crypto.createHmac('sha256', config.secrets.hmacSecret);
            hmac.update(cookie["username"] + ':' + cookie["token"])
            let localHmac = hmac.digest('hex');

            // Compare generated hmac to given hmac
            if (localHmac !== cookie["hmac"]) {
                res.cookie("rememberMe", '');
                req.session.error = "Un cookie irrégulier a été détécté. L'incident a été enregistré et rapporté";
                return 0;
            }
        });
    } else {
        console.log("empty cookie");
        req.session.error = "Veuillez vous connecter";
        return 0;
    } // END OF IF COOKIES

    return 1;
}

module.exports = {
    generatePwd,
    sendWelcomeMail,
    sendReportMail,
    sendNewPassword,
    resetId,
    generateToken,
    checkHmac,
    checkToken,
    getUsername,
    authenticate
}
