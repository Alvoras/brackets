const fs = require('fs');

const db = require('../modules/db');
const userManagement = require('../modules/userManagement');
const connection = db.connect();
const categories = JSON.parse(fs.readFileSync(__dirname + '/../config/categories.json'));
const config = JSON.parse(fs.readFileSync(__dirname + '/../config/config.json'));
const sanitizeHtml = require('sanitize-html');
const markdownit = require('markdown-it');

const params = {
    html: true,
    breaks: false,
    typographer: true
};
const md = markdownit(params);

function parseDate(date) {
    if (date === "0000-00-00 00:00:00") {
        return date;
    }

    let creationDate = date.toJSON();
    let parsedDate = creationDate.slice(0, 10).split("-").reverse().join("/").concat(' à ').concat(creationDate.slice(11, 16));

    return parsedDate;
}

function loadThreadContent2(req, res, threadId, currentUser) {
    db.getUserInfo(connection, username).then((userData) => {
        let userId = userData.id;
        let isMod = userData.is_mod;
        db.getInfo(connection, "threads", threadId).then((threadData) => {

            let i = 0;
            let j = 0;
            // If threadData is undefined then signal thread not found
            let dataLength = threadData.length;
            threadData.created = parseDate(threadData.created);

            // Parse markdown
            threadData.description = md.render(threadData.description);

            db.getInfo(connection, "users", threadData.author_id).then((opData) => {
                db.getClass(connection, threadData.author_id).then((opClass) => {
                    opData.className = opClass.name;
                    opData.classIcon = opClass.icon;

                    db.getInfo(connection, "roles", opData.role_id).then((roleInfo) => {
                        opData.role_title = roleInfo.title;
                        opData.icon = roleInfo.icon;
                        opData.role_rarity = roleInfo.rarity;

                        db.getUserNotifs(connection, userId).then((notifications) => {
                            let notifLength = notifications.length;
                            for (i = 0; i < notifLength; i++) {
                                notifications[i].created = parseDate(notifications[i].created);
                            }

                            db.getThreadMessages(connection, threadId).then((threadMessages) => {
                                let postQuantity = threadMessages.length;
                                let hasVotedThread = false;
                                let hasVotedMessage = false;
                                let messagesData = {};

                                for (i = 0; i < postQuantity; i++) {
                                    threadMessages[i].text = sanitizeHtml(threadMessages[i].text, {
                                        exclusiveFilter: (frame) => {
                                            return frame.tag === 'style' && frame.tag === 'script';
                                        }
                                    });

                                    messagesData[i] = threadMessages[i];

                                    // Parse markdown
                                    messagesData[i].text = md.render(messagesData[i].text);

                                    messagesData[i].created = parseDate(threadMessages[i].created);

                                    messagesData[i].last_modified = parseDate(threadMessages[i].last_modified);
                                }

                                let detailsQuery = "";
                                let usersData = {};
                                i = 0;

                                // START RECURSIVE MYSQL QUERIES
                                (function detailsRecursive(i) {

                                    // IF FIRST RECURSIVE LOOP COUNTER HAS REACHED LIMIT
                                    if (i === postQuantity) {
                                        threadData.score = 0;
                                        db.getThreadScoreData(connection, threadId).then((threadScoreData) => {
                                            hasVotedThread = false;
                                            for (i = 0; i < threadScoreData.length; i++) {
                                                threadData.score += threadScoreData[i].vote;
                                                hasVotedThread = (threadScoreData[i].user_id === userId) ? true : false;
                                                if (hasVotedThread) {
                                                    if (threadScoreData[i].vote === 1) {
                                                        threadData.voteTypeThread = "upvote";
                                                    } else if (threadScoreData[i].vote === 0) {
                                                        threadData.voteTypeThread = null;
                                                    } else {
                                                        threadData.voteTypeThread = "downvote";
                                                    }
                                                }
                                            }
                                            return res.render('thread', {
                                                title: threadData.title,
                                                threadData,
                                                threadId,
                                                opData,
                                                messagesData,
                                                usersData,
                                                postQuantity,
                                                currentUser,
                                                dataLength,
                                                notifLength,
                                                notifications,
                                                username,
                                                isMod
                                            });
                                        }).catch((err) => setImmediate(() => {
                                            throw err;
                                        })); // END OF getThreadScore QUERY
                                    } else if (i < postQuantity) {

                                        db.getInfo(connection, "users", messagesData[i].author_id).then((userInfo) => {
                                            usersData[i] = userInfo;

                                            db.getInfo(connection, "roles", usersData[i].role_id).then((roleInfo) => {
                                                usersData[i].role_title = roleInfo.title;
                                                usersData[i].icon = roleInfo.icon;
                                                usersData[i].role_rarity = roleInfo.rarity;


                                                db.getClass(connection, usersData[i].id).then((classesData) => {
                                                    usersData[i].className = classesData.name;
                                                    usersData[i].classIcon = classesData.icon;

                                                    db.getMessageVotes(connection, messagesData[i].id, threadData.id).then((messageScoreData) => {
                                                        hasVotedMessage = false;
                                                        messagesData[i].score = 0;
                                                        for (j = 0; j < messageScoreData.length; j++) {
                                                            messagesData[i].score += messageScoreData[j].vote;
                                                        }

                                                        if (messageScoreData !== undefined) {
                                                            for (j = 0; j < messageScoreData.length; j++) {
                                                                hasVotedMessage = (messageScoreData[j].user_id === userId) ? true : false;
                                                                if (hasVotedMessage) {
                                                                    if (messageScoreData[j].vote === 1) {
                                                                        usersData[i].voteTypeMessage = "upvote";
                                                                    } else if (messageScoreData[j].vote === 0) {
                                                                        usersData[i].voteTypeMessage = null;
                                                                    } else {
                                                                        usersData[i].voteTypeMessage = "downvote";
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        detailsRecursive(i + 1);
                                                    }).catch((err) => setImmediate(() => {
                                                        throw err;
                                                    })); // END OF getMessageScore PROMISE
                                                }).catch((err) => setImmediate(() => {
                                                    throw err;
                                                })); // END OF getClass PROMISE
                                            }).catch((err) => setImmediate(() => {
                                                throw err;
                                            })); // END OF getInfo roles PROMISE
                                        }).catch((err) => setImmediate(() => {
                                            throw err;
                                        })); // END OF getInfo users PROMISE
                                    } // if i < postQuantity
                                })(0); // END OF RECURSIVE LOOP
                            }).catch((err) => setImmediate(() => {
                                throw err;
                            })); // END OF getThreadMessages PROMISE
                        }).catch((err) => setImmediate(() => {
                            throw err;
                        })); // END OF getUserNotifs PROMISE
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF getInfo roles PROMISE
                }).catch((err) => setImmediate(() => {
                    throw err;
                })); // END OF getClass roles PROMISE
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF getInfo users PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getInfo threads PROMISE
    }).catch((err) => setImmediate(() => {
        throw err;
    })); // END OF getUserInfo PROMISE
}
/*
function loadThreadContent(req, res, threadId, currentUser) {

    db.getUserInfo(connection, username).then( (userData) => {
        let userId = userData.id;
        let isMod = userData.is_mod;
        db.getInfo(connection, "threads", threadId).then( (threadData) => {
          let i = 0;
          let dataLength = threadData.length;
          threadData.created = parseDate(threadData.created);

          db.getInfo(connection, "users", threadData.author_id).then( (opData) => {
              db.getClass(connection, threadData.author_id).then( (opClass) => {
                  opData.className = opClass.name;
                  opData.classIcon = opClass.icon;

                  db.getInfo(connection, "roles", opData.role_id).then( (roleInfo) => {
                    opData.role_title = roleInfo.title;
                    opData.icon = roleInfo.icon;
                    opData.role_rarity = roleInfo.rarity;

                    db.getUserNotifs(connection, userId).then( (notifications) => {
                      let notifLength = notifications.length;
                      for (k = 0; k < notifLength; k++) {
                          notifications[k].created = parseDate(notifications[k].created);
                      }

                        db.getThreadMessages(connection, threadId).then( (threadMessages) => {
                          let postQuantity = threadMessages.length;
                          let j = 0;
                          let k = 0;
                          let l = 0;
                          let m = 0;
                          let hasVotedThread = false;
                          let hasVotedMessage = false;
                          let messagesData = {};

                          for (i = 0; i < postQuantity; i++) {
                            messagesData[i] = threadMessages[i];
                            threadMessages[i].text = sanitizeHtml(threadMessages[i].text, {
                                exclusiveFilter: (frame) => {
                                    return frame.tag !== 'style' || frame.tag !== 'script'
                                }
                            });

                            messagesData[i].created = parseDate(threadMessages[i].created);
                            messagesData[i].last_modified = parseDate(threadMessages[i].last_modified);
                          }

                          let detailsQuery = "";
                          let usersData = {};
                          i = 0;

                          // START RECURSIVE MYSQL QUERIES
                          //  -> WE WRAP THE FUNCTION WITH PARENTHESIS TO IMMEDIATELY TRIGGER IT
                          //     AND TO BE ABLE TO NAME IT IN ORDER TO CALL IT RECURSIVELY (FROM WITHIN)
                          (function detailsRecursive(i) {

                            // IF FIRST RECURSIVE LOOP COUNTER HAS REACHED LIMIT
                            // LAUNCH SECOND LOOP
                            if (i === postQuantity) {

                              // SECOND RECURSIVE LOOP STARTED WHEN THE FIRST ONE END
                              (function rolesRecursive(j) {

                                // WHEN SECOND LOOP END, SEND RESPONSE BACK TO CLIENT
                                if (j === postQuantity){

                                  (function classesRecursive(k) {
                                    // WHEN THIRD LOOP END, SEND RESPONSE BACK TO CLIENT
                                    if (k === postQuantity){

                                        (function scoreRecursive(l) {
                                          // IF FOURTH RECURSIVE LOOP COUNTER HAS REACHED LIMIT
                                          // THEN
                                          if (l === postQuantity) {
                                              threadData.score = 0;
                                              db.getThreadScoreData(connection, threadId).then( (threadScoreData) => {
                                                  hasVotedThread = false;
                                                  for (i = 0; i < threadScoreData.length; i++) {
                                                      console.log("USERID", userId);
                                                      threadData.score += threadScoreData[i].vote;
                                                      hasVotedThread = (threadScoreData[i].user_id === userId)?true:false;
                                                      console.log(threadScoreData[i].user_id, " =?= ",userId);
                                                      if (hasVotedThread) {
                                                          if (threadScoreData[i].vote === 1) {
                                                              threadData.voteTypeThread = "upvote";
                                                          }else if (threadScoreData[i].vote === 0) {
                                                              threadData.voteTypeThread = null;
                                                          }else {
                                                          threadData.voteTypeThread = "downvote";
                                                        }
                                                      }
                                                  }
                                                  console.log(threadMessages);
                                                  console.log(messagesData);
                                                  return res.render('thread', {
                                                                                  title: threadData.title,
                                                                                  threadData,
                                                                                  threadId,
                                                                                  opData,
                                                                                  messagesData,
                                                                                  usersData,
                                                                                  postQuantity,
                                                                                  currentUser,
                                                                                  dataLength,
                                                                                  notifLength,
                                                                                  notifications,
                                                                                  username,
                                                                                  isMod
                                                                              });
                                              }).catch( (err) => setImmediate( () => { throw err; })); // END OF getThreadScore QUERY
                                          }

                                          if (l < postQuantity) {
                                            // QUERY FOURTH SET OF DATA
                                            db.getMessageVotes(connection, messagesData[l].id, threadData.id).then( (messageScoreData) => {
                                                hasVotedMessage = false;
                                                messagesData[l].score = 0;
                                                for (i = 0; i < messageScoreData.length; i++) {
                                                    messagesData[l].score += messageScoreData[i].vote;
                                                }

                                                if (messageScoreData !== undefined) {
                                                    for (i = 0; i < messageScoreData.length; i++) {
                                                        hasVotedMessage = (messageScoreData[i].user_id === userId)?true:false;
                                                        console.log(messageScoreData[i].user_id, " =?= ",userId);
                                                        if (hasVotedMessage) {
                                                            if (messageScoreData[i].vote === 1) {
                                                                usersData[l].voteTypeMessage = "upvote";
                                                            }else if (messageScoreData[i].vote === 0) {
                                                                usersData[l].voteTypeMessage = null;
                                                            }else {
                                                            usersData[l].voteTypeMessage = "downvote";
                                                          }
                                                        }
                                                    }
                                                }

                                              // ITERATE FOURTH LOOP IN THE QUERY CALLBACK FUNCTION
                                              scoreRecursive(l+1);
                                          }).catch( (err) => setImmediate( () => { throw err; })); // END OF getMessageScore PROMISE
                                          }
                                        })(0);
                                    }

                                    if (k < postQuantity) {
                                        // QUERY THIRD SET OF DATA USING PREVIOUS RESULTS
                                        db.getClass(connection, usersData[k].id).then( (classesData) => {
                                          usersData[k].className = classesData.name;
                                          usersData[k].classIcon = classesData.icon;

                                          // ITERATE THIRD LOOP IN CALLBACK
                                          classesRecursive(k+1);
                                      }); // END OF CLASSES QUERY
                                    }
                                  })(0);
                                }

                                if (j < postQuantity) {
                                    // QUERY SECOND SET OF DATA USING PREVIOUS RESULTS
                                    db.getInfo(connection, "roles", usersData[j].role_id).then( (roleInfo) => {
                                      usersData[j].role_title = roleInfo.title;
                                      usersData[j].icon = roleInfo.icon;
                                      usersData[j].role_rarity = roleInfo.rarity;

                                      // ITERATE SECOND LOOP IN CALLBACK
                                      rolesRecursive(j+1);
                                    }); // END OF ROLE QUERY
                                }
                              })(0);
                            }

                            if (i < postQuantity) {

                              // QUERY FIRST SET OF DATA
                              db.getInfo(connection, "users", messagesData[i].author_id).then( (userInfo) => {
                                usersData[i] = userInfo;

                                // ITERATE FIRST LOOP IN THE QUERY CALLBACK FUNCTION
                                detailsRecursive(i+1);
                              }); // END OF DETAILS QUERY
                            }
                          })(0); // END OF RECURSIVE LOOP
                      }).catch( (err) => setImmediate( () => { throw err; })); // END OF getThreadInfo messages QUERY
                    }).catch( (err) => setImmediate( () => { throw err; })); // END OF getUserNotifs PROMISE
                  }).catch( (err) => setImmediate( () => { throw err; })); // END OF getInfo role PROMISE END OF ROLE QUERY
              }).catch( (err) => setImmediate( () => { throw err; })); // END OF POSTS QUERY
            }).catch( (err) => setImmediate( () => { throw err; })); // END OF getInfo users PROMISE END OF OP QUERY
        }).catch( (err) => setImmediate( () => { throw err; })); // END OF getInfo messages PROMISE END OF THREAD QUERY
    }).catch( (err) => setImmediate( () => { throw err; })); // END OF getUserId PROMISE
} // END OF LOADTHREAD()
*/
function loadThreadWall(req, res) {
    let sortMode = req.params.sortMode;
    let page = parseInt(req.params.page);
    let category = req.params.category;

    let currentUser = userManagement.getUsername(req);

    let noResults = 1;

    db.getThreadsCount(connection, category).then((threadCount) => {
        let maxPage = threadCount / 20;

        db.getThreads(connection, sortMode, page, category).then((threadData) => {
            if (threadData.length > 0) noResults = 0;

            let i = 0;
            let j = 0;
            let k = 0;
            let dataLength = threadData.length;

            let data = threadData;
            let threadQuantity = threadData.length;
            let currentUser = userManagement.getUsername(req);

            db.getUserInfo(connection, currentUser).then((userData) => {
                let userId = userData.id;
                let isMod = userData.is_mod;
                db.getUserNotifs(connection, userId).then((notifications) => {
                    let notifLength = notifications.length;
                    for (k = 0; k < notifLength; k++) {
                        notifications[k].created = parseDate(notifications[k].created);
                    }
                    (function dbQueryRecursive(i) {
                        // IF FIRST RECURSIVE LOOP COUNTER HAS REACHED LIMIT
                        // THEN
                        if (i === threadQuantity) {
                            let sortModeDisplay = "Plus récent"; // default

                            switch (sortMode) {
                                case "best":
                                    sortModeDisplay = "Meilleur";
                                    break;
                                case "vote":
                                    sortModeDisplay = "Plus de vote";
                                    break;
                                default:

                            }

                            return res.render('index', {
                                title: config.title,
                                dataLength,
                                data,
                                userData,
                                categories,
                                currentUser,
                                notifications,
                                notifLength,
                                isMod,
                                noResults,
                                sortModeDisplay,
                                page,
                                maxPage,
                                sortMode,
                                category
                            });
                        }
                        db.checkFavorite(connection, data[i].id, userId).then((favResults) => {
                            if (favResults !== undefined) {
                                data[i].isFav = favResults.is_active;
                            } else {
                                data[i].isFav = 0;
                            }
                            db.checkSub(connection, data[i].id, userId).then((subResults) => {
                                if (subResults !== undefined) {
                                    data[i].isSub = subResults.is_active;
                                } else {
                                    data[i].isSub = 0;
                                }
                                db.getThreadScoreData(connection, data[i].id).then((threadScoreData) => {
                                    hasVotedThread = false;
                                    data[i].score = 0;
                                    for (j = 0; j < threadScoreData.length; j++) {

                                        data[i].score += threadScoreData[j].vote;
                                        hasVotedThread = (threadScoreData[j].user_id === userId) ? true : false;
                                        if (hasVotedThread) {
                                            if (threadScoreData[j].vote === 1) {
                                                data[i].voteTypeThread = "upvote";
                                            } else if (threadScoreData[j].vote === 0) {
                                                data[i].voteTypeThread = null;
                                            } else {
                                                data[i].voteTypeThread = "downvote";
                                            }
                                        }
                                    }
                                    db.getVoteCount(connection, data[i].id).then((voteCount) => {
                                        data[i].voteCount = voteCount;

                                        db.getAnswerCount(connection, data[i].id).then((answerCount) => {
                                            data[i].answerCount = answerCount;

                                            // ITERATE FIRST LOOP IN THE QUERY CALLBACK FUNCTION
                                            dbQueryRecursive(i + 1);
                                        }).catch((err) => setImmediate(() => {
                                            throw err;
                                        })); // END OF getAnswerCount QUERY
                                    }).catch((err) => setImmediate(() => {
                                        throw err;
                                    })); // END OF getVoteCount QUERY
                                }).catch((err) => setImmediate(() => {
                                    throw err;
                                })); // END OF getThreadScoreData QUERY
                            }).catch((err) => setImmediate(() => {
                                throw err;
                            })); // END OF checkSub PROMISE
                        }).catch((err) => setImmediate(() => {
                            throw err;
                        })); // END OF checkFavorite PROMISE
                    })(0); // END OF RECURSIVE LOOP
                }).catch((err) => setImmediate(() => {
                    throw err;
                })); // END OF getUserNotifs PROMISE
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF getUserInfo PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getThreads PROMISE
    }).catch((err) => setImmediate(() => {
        throw err;
    })); // END OF getThreadsCount PROMISE
}

function loadThreadWallBySearch(req, res, page, searchTerms) {
    let currentUser = userManagement.getUsername(req);

    let sortMode = req.params.sortMode;

    db.findThreads(connection, page, searchTerms).then((threadData) => {
        let i = 0;
        let j = 0;
        let dataLength = threadData.length;
        let data = threadData;
        let threadQuantity = threadData.length;
        let currentUser = userManagement.getUsername(req);

        db.getUserInfo(connection, currentUser).then((userData) => {
            let userId = userData.id;
            let isMod = userData.is_mod;

            db.getUserNotifs(connection, userId).then((notifications) => {
                let notifLength = notifications.length;
                for (k = 0; k < notifLength; k++) {
                    notifications[k].created = parseDate(notifications[k].created);
                }
                (function dbQueryRecursive(i) {
                    // IF FIRST RECURSIVE LOOP COUNTER HAS REACHED LIMIT
                    // THEN
                    if (i === threadQuantity) {
                        res.render('index', {
                            title: config.title,
                            dataLength,
                            data,
                            categories,
                            currentUser,
                            isMod,
                            notifications,
                            notifLength
                        });
                        return; // NEEDED TO STOP THE LOOP ?
                    }
                    db.checkFavorite(connection, data[i].id, userId).then((favResults) => {
                        if (favResults !== undefined) {
                            data[i].isFav = favResults.is_active;
                        } else {
                            data[i].isFav = 0;
                        }
                        db.checkSub(connection, data[i].id, userId).then((subResults) => {
                            if (subResults !== undefined) {
                                data[i].isSub = subResults.is_active;
                            } else {
                                data[i].isSub = 0;
                            }
                            db.getThreadScoreData(connection, data[i].id).then((threadScoreData) => {
                                hasVotedThread = false;
                                data[i].score = 0;
                                for (j = 0; j < threadScoreData.length; j++) {
                                    data[i].score += threadScoreData[j].vote;
                                    hasVotedThread = (threadScoreData[j].user_id === userId) ? true : false;
                                    if (hasVotedThread) {
                                        if (threadScoreData[j].vote === 1) {
                                            data[i].voteTypeThread = "upvote";
                                        } else if (threadScoreData[j].vote === 0) {
                                            data[i].voteTypeThread = null;
                                        } else {
                                            data[i].voteTypeThread = "downvote";
                                        }
                                    }
                                }
                                db.getVoteCount(connection, data[i].id).then((voteCount) => {
                                    data[i].voteCount = voteCount;

                                    db.getAnswerCount(connection, data[i].id).then((answerCount) => {
                                        data[i].answerCount = answerCount;

                                        // ITERATE FIRST LOOP IN THE QUERY CALLBACK FUNCTION
                                        dbQueryRecursive(i + 1);
                                    }).catch((err) => setImmediate(() => {
                                        throw err;
                                    })); // END OF getAnswerCount QUERY
                                }).catch((err) => setImmediate(() => {
                                    throw err;
                                })); // END OF getVoteCount QUERY
                            }).catch((err) => setImmediate(() => {
                                throw err;
                            })); // END OF getThreadScoreData QUERY
                        }).catch((err) => setImmediate(() => {
                            throw err;
                        })); // END OF checkSub PROMISE
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF checkFavorite PROMISE
                })(0); // END OF RECURSIVE LOOP
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF getUserNotifs PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getUserInfo PROMISE
    }).catch((err) => setImmediate(() => {
        throw err;
    })); // END OF getThreads PROMISE
}

module.exports = {
    parseDate,
    loadThreadContent2,
    loadThreadWall,
    loadThreadWallBySearch
}
