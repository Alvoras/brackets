$(document).ready(() => {
    let keyupCounter = 0;

    if ($("#preview").length !== 0) {
        let sanitize = true;
        updatePreview($("#reply-box"), sanitize);
    }

    let editBuffer = '';

    // SOCKET.IO
    socket.on('newReply', (threadId, lastId, userData, text, isBroadcast, currentUser) => {
        let currentThreadId = getThreadId();

        // If the current client is not in the concerned thread
        if (currentThreadId !== threadId) {
            return
        }

        appendReply(threadId, lastId, userData, text, isBroadcast, currentUser);
        $("#reply-box").val('');
        $("#preview").html('<p id="preview-text"><span id="preview-placeholder">Vide</span></p>');
    });

    socket.on('messageValidated', (threadId, postId) => {
        markAsChosen(threadId, postId);
    });

    socket.on('fromMessageThreadUpdated', (threadId, messageId, values) => {
        let currentThreadId = getThreadId();

        // If the current client is not in the concerned thread
        if (currentThreadId !== threadId) {
            return;
        }

    });

    socket.on('reportSent', () => {
        // Diplay notification
        Materialize.toast('Post signalé', 4000);
        $(".toast").css('background', '#7FC468');
    });

    $("body").click((e) => {
        let target = $(e.target);
        if (target.attr("id") === "new-reply-button") {
            e.preventDefault();

            let currentThreadId = getThreadId();
            let currentMessageId = getLatestPostId();
            let threadTitle = getThreadTitle();
            let threadOwner = getThreadOwnerName();

            let sanitize = true;

            let text = mdToHtml($("#reply-box").val(), sanitize);

            if ($("#reply-box").val().trim().length == 0) {
                displayEmptyWarning();

                return;
            }

            socket.emit("sendReply", currentThreadId, currentMessageId, threadTitle, threadOwner, text);
        } else if (target.hasClass("upvote")) {
            let messageId = getPostId(e);
            let threadId = getThreadId(e);
            let voteMode = "upvote";

            let hadPreviouslyVoted = getCurrentVote(messageId);

            socket.emit('voteMessage', messageId, threadId, voteMode, hadPreviouslyVoted);
        } else if (target.hasClass("downvote")) {
            let messageId = getPostId(e);
            let threadId = getThreadId(e);
            let voteMode = "downvote";

            let hadPreviouslyVoted = getCurrentVote(messageId);

            socket.emit('voteMessage', messageId, threadId, voteMode, hadPreviouslyVoted);
        } else if (target.hasClass("edit-button")) {
            editBuffer = editReply(e);
        } else if (target.hasClass("confirm-edit")) {
            sendEdit(e);
        } else if (target.hasClass("cancel-edit")) {
            cancelEdit(e, editBuffer);
        } else if (target.hasClass("valid-reply-button")) {
            let currentThreadId = getThreadId();
            let postId = getPostId(e);
            let chosenName = $("#" + postId).find(".member-name").html();

            socket.emit("validateMessage", currentThreadId, postId, chosenName);
        } else if (target.hasClass("upvote")) {
            let messageId = getPostId(e);
            let threadId = getThreadId(e);
            let voteMode = "upvote";

            let hadPreviouslyVoted = getCurrentVote(messageId);

            socket.emit('voteMessage', messageId, threadId, voteMode, hadPreviouslyVoted);
        } else if (target.hasClass("downvote")) {
            let messageId = getPostId(e);
            let threadId = getThreadId(e);
            let voteMode = "downvote";

            let hadPreviouslyVoted = getCurrentVote(messageId);

            socket.emit('voteMessage', messageId, threadId, voteMode, hadPreviouslyVoted);
        } else if (target.hasClass("report-button")) {
            reportPost(e);
        }
    });

    // Update preview every 5 keystroke (deprecated, see below)
    /*$("body").keyup((e) => {
        if ($(e.target).attr("id") === "reply-box") {
            let keystrokeMax = 5;
            if (keyupCounter%keystrokeMax != 0) {
                keyupCounter++;
                return;
            }
            let commentErrorText = document.getElementById('comment-error-text');
            let commErTxtClasses = commentErrorText.getAttribute('class');

            if (commErTxtClasses.indexOf('display-error-text' === -1)) {
                commentErrorText.className = 'error-text';
            }

            keyupCounter++;

            let sanitize = false;
            updatePreview($("#reply-box"), sanitize);
        }
    });*/

    $("#refresh-preview").click(() => {
        clearErrorMessages();

        let sanitize = true;
        updatePreview($("#reply-box"), sanitize);
    });

    // MOD TOOLS
    $(".mod-lock-container").click(() => {
        let threadId = getThreadId();
        let values = {};

        if ($("#thread-container").hasClass("locked")) {
            values = {
                is_closed: 0
            };
        } else {
            values = {
                is_closed: 1
            };
        }

        socket.emit('updateThreadFromMessage', values, threadId);
    });

    socket.on('fromMessageThreadUpdated', (threadId, values) => {
        let currentThreadId = getThreadId();

        // If the current client is not in the concerned thread
        if (currentThreadId !== threadId) {
            return
        }

        let threadContainer = $("#thread-container");

        /*  if ( (values.is_deleted) ) {
            window.location.href = ('/');

          }else */
        if (values.is_closed !== undefined) {
            if ($(".mod-lock-container").length !== 0) {

                if ((values.is_closed)) {
                    $(".mod-lock-container").addClass("locked-button");
                } else if (!(values.is_closed)) {
                    $(".mod-lock-container").removeClass("locked-button");
                }
            }

            if ((values.is_closed)) {
                threadContainer.addClass("locked");

                $("#comment-section-container").html("");
                $("#add-comment").html("Le sujet a été fermé.")

                Materialize.toast('Sujet verrouillé', 2000);
                $(".toast").css('background', '#7FC468');
            } else if (!(values.is_closed)) {
                threadContainer.removeClass("locked");

                let commentForm = '<form id="new-reply-form" method="post">\
                            <textarea name="replyText" id="reply-box" rows="8" cols="80"></textarea>\
                            <div id="reply-button-container">\
                              <button id="new-reply-button" name="new-reply-button">Envoyer</button>\
                            </div>\
                        </form>\
                        <h3 id="preview-label">Preview</h3>\
                        <div id="preview-box-container">\
                          <div id="preview">\
                            <p id="preview-text"><span id="preview-placeholder">Vide</span></p>\
                          </div>\
                        </div>';
                $("#comment-section-container").html(commentForm);
                $("#add-comment").html("AJOUTER UN COMMENTAIRE");

                Materialize.toast('Sujet déverrouillé', 2000);
                $(".toast").css('background', '#7FC468');
            }
        }
    });


    /*  $("#new-reply-button").click( (e) => {
        e.preventDefault();

        let currentThreadId = getThreadId();
        let text = $("#preview").html();

        socket.emit("sendReply", currentThreadId, text);
    });

    $(".valid-reply-button").click( (e) => {
        let currentThreadId = getThreadId();
        let postId = getPostId(e);
        console.log(postId);
        let chosenName = $("#"+postId).find(".member-name").html();

        socket.emit("validateMessage", currentThreadId, postId, chosenName);
    });
*/
    // EDIT
    socket.on('postUpdated', (postId, text) => {

        applyEdit(postId, text);
    });

    /*$(".bottom-reply-container").click( (e) => {
        if (e.target.className === "edit-button") {
            editBuffer = editReply(e);
        }else if(e.target.className === "confirm-edit"){
            sendEdit(e);
        }else if (e.target.className === "cancel-edit") {
            cancelEdit(e, editBuffer);
        }
    });*/

    // VOTES
    $(".thread-upvote").click((e) => {
        let threadId = getThreadId(e);
        let voteMode = "upvote";

        hadPreviouslyVoted = getCurrentOPVote();
        socket.emit('voteThread', threadId, voteMode, hadPreviouslyVoted);
    });

    $(".thread-downvote").click((e) => {
        let threadId = getThreadId(e);
        let voteMode = "downvote";

        hadPreviouslyVoted = getCurrentOPVote();
        socket.emit('voteThread', threadId, voteMode, hadPreviouslyVoted);
    });

    socket.on('voteThreadDone', (threadId, newVote, isBroadcast, hadPreviouslyVoted) => {
        let currentThreadId = getThreadId();

        // If the current client is not in the concerned thread
        if (currentThreadId !== threadId) {
            return
        }

        let voteMode = (newVote === 1) ? "thread-upvote" : "thread-downvote";
        let voteIcons = $("#original-post .material-icons");
        let scoreDisplay = $("#original-post .score-display");
        let currentScore = parseInt(scoreDisplay.html());

        // Default values
        let voteValue = 1;
        let cancelVote = false;

        if (!(isBroadcast)) {
            if (voteIcons.hasClass("voted")) {
                let currentVote = (($("#original-post .thread-upvote").hasClass("voted"))) ? "thread-upvote" : "thread-downvote";
                voteIcons.removeClass("voted");


                console.log(voteMode, currentVote);
                if (currentVote !== voteMode) {
                    highlightOPVoteArrow(voteMode);
                    // If the user changes his mind, we need to compensate
                    // for the previous vote he made
                    voteValue = 2;
                } else {
                    // Tells the function to substract instead of add and vice versa
                    cancelVote = true;
                }

                updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue, cancelVote);
            } else {
                highlightOPVoteArrow(voteMode);
                updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue);
            }
        } else {
            if ((hadPreviouslyVoted.isTrue)) {
                if (hadPreviouslyVoted.previousVote !== voteMode) {
                    // If the user changes his mind, we need to compensate
                    // for the previous vote he made
                    voteValue = 2;
                } else {
                    // Tells the function to substract instead of add and vice versa
                    cancelVote = true;
                }

                updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue, cancelVote);
            } else {
                updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue);
            }
        }

    });

    /*
    // Doesn't work with a newly created post
    // -> Event not bound to JS-generated DOM element
    $(".upvote").click( (e) => {
        let messageId = getPostId(e);
        let threadId = getThreadId(e);
        let voteMode = "upvote";

        hadPreviouslyVoted = getCurrentVote(messageId);

        socket.emit('voteMessage', messageId, threadId, voteMode, hadPreviouslyVoted);
    });

    $(".downvote").click( (e) => {
        let messageId = getPostId(e);
        let threadId = getThreadId(e);
        let voteMode = "downvote";

        hadPreviouslyVoted = getCurrentVote(messageId);

        socket.emit('voteMessage', messageId, threadId, voteMode, hadPreviouslyVoted);
    });*/

    socket.on('voteMessageDone', (threadId, messageId, newVote, isBroadcast, hadPreviouslyVoted) => {
        let currentThreadId = getThreadId();

        // If the current client is not in the concerned thread
        if (currentThreadId !== threadId) {
            return
        }

        let voteMode = (newVote === 1) ? "upvote" : "downvote";
        let voteIcons = $("#" + messageId + " .material-icons");
        let scoreDisplay = $("#" + messageId + " .score-display");
        let currentScore = parseInt(scoreDisplay.html());

        // Default values
        let voteValue = 1;
        let cancelVote = false;

        if (!(isBroadcast)) {
            if (voteIcons.hasClass("voted")) {
                let currentVote = (($("#" + messageId + " .upvote").hasClass("voted"))) ? "upvote" : "downvote";
                voteIcons.removeClass("voted");


                if (currentVote !== voteMode) {
                    highlightMessageVoteArrow(messageId, voteMode);
                    // If the user changes his mind, we need to compensate
                    // for the previous vote he made
                    voteValue = 2;
                } else {
                    // Tells the function to substract instead of add and vice versa
                    cancelVote = true;
                }

                updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue, cancelVote);
            } else {
                highlightMessageVoteArrow(messageId, voteMode);
                updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue);
            }
        } else {
            if ((hadPreviouslyVoted.isTrue)) {
                if (hadPreviouslyVoted.previousVote !== voteMode) {
                    // If the user changes his mind, we need to compensate
                    // for the previous vote he made
                    voteValue = 2;
                } else {
                    // Tells the function to substract instead of add and vice versa
                    cancelVote = true;
                }

                updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue, cancelVote);
            } else {
                updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue);
            }
        }
    });
});
