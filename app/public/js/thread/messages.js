function markAsChosen(threadId, postId) {
    let currentThreadId = getThreadId();

    if (currentThreadId === threadId) {
        let post = $("#" + postId);

        post.addClass("highlight-validated-post");
        post.find("button").addClass("validated");

        $(".valid-button-container").css("display", "none");
    }
}

function reportPost(e) {
    let postId = getPostId(e);

    socket.emit('reportPost', postId);
}

function editReply(e) {
    let editButton = $(e.target);
    let postId = getPostId(e);
    let replyContainer = document.getElementById(postId);
    let postResponseContainer = replyContainer.getElementsByClassName('post-response')[0];
    let replyContent = postResponseContainer.innerHTML;
    let confirmEditButton = '<p class="confirm-edit edit-action-button">Valider</p><p class="cancel-edit edit-action-button">Annuler</p>';

    let parseMarkdown = toMarkdown(replyContent);
    let sanereplyContenant = sanitizeHtml(parseMarkdown, {
        allowedTags: []
    });

    $(postResponseContainer).replaceWith('<textarea class="edit-textarea">' + sanereplyContenant + '</textarea>');
    editButton.replaceWith(confirmEditButton);

    return replyContent;
}

function sendEdit(e) {
    let confirmButton = $(e.target);
    let postId = getPostId(e);
    let editTextarea = $("#" + postId + " .edit-textarea");
    let editContent = editTextarea.val();
    let editButton = '<p class="edit-button">Edit</p>';

    // MARKDOWN PARSER
    let params = {
        html: true,
        breaks: false,
        typographer: true
    };
    let md = window.markdownit(params);

    let htmlText = parseHTML(editContent);
    /*let saneHtml = window.sanitizeHtml(htmlText, {
          // Add img to the whitelist
        allowedTags: sanitizeHtml.defaults.allowedTags.concat([ 'img' ])
      });*/
    let mdContent = md.render(htmlText);

    socket.emit('updatePost', postId, mdContent);
}

function applyEdit(postId, text) {
    let editButton = '<p class="edit-button">Edit</p>';
    let editTextarea = $("#" + postId + " .edit-textarea");

    if ((editTextarea.length)) {
        editTextarea.replaceWith('<div class="post-response">' + text + '</div>');

        $("#" + postId + " .confirm-edit").replaceWith(editButton);
        $("#" + postId + " .cancel-edit").remove();

        // Diplay notification
        Materialize.toast('Réponse mise à jour', 4000);
        $(".toast").css('background', '#7FC468');
    } else {
        $("#" + postId).find(".post-response").replaceWith('<div class="post-response">' + text + '</div>');
    }

}

function cancelEdit(e, editBuffer) {
    let cancelButton = $(e.target);
    let postId = getPostId(e);
    let editTextarea = $("#" + postId + " .edit-textarea");
    let editButton = '<p class="edit-button">Edit</p>';

    editTextarea.replaceWith('<div class="post-response">' + editBuffer + '</div>');
    cancelButton.replaceWith(editButton);
    $("#" + postId + " .confirm-edit").remove();
}

function getPostId(e) {
    return $(e.target).closest(".post-container").attr("id");
}

function getLatestPostId() {
    return $(".post-container").last().attr("id");
}

function getThreadId() {
    let urlArray = window.location.pathname.split('/');
    let currentThreadId = urlArray[urlArray.length - 2];

    return currentThreadId;
}

function getThreadTitle() {
    return $("#original-post").find("#op-title").html();
}

function getThreadOwnerName() {
    return $("#original-post").find(".member-name").html().trim();
}

function getCurrentOPVote() {
    let voteIcons = $("#original-post .material-icons");
    let hadPreviouslyVoted = {
        isTrue: false,
        previousVote: null
    };

    if (voteIcons.hasClass("voted")) {
        hadPreviouslyVoted.isTrue = true;
        let currentVote = (($("#original-post .thread-upvote").hasClass("voted"))) ? "thread-upvote" : "thread-downvote";
        hadPreviouslyVoted.previousVote = currentVote;
    }

    return hadPreviouslyVoted;
}

function getCurrentVote(id) { // Can be either message ID (in-thread) or thread ID (front page)
    let voteIcons = $("#" + id + " .material-icons");
    let hadPreviouslyVoted = {
        isTrue: false,
        previousVote: null
    };

    if (voteIcons.hasClass("voted")) {
        hadPreviouslyVoted.isTrue = true;
        let currentVote = (($("#" + id + " .upvote").hasClass("voted"))) ? "upvote" : "downvote";
        hadPreviouslyVoted.previousVote = currentVote;
    }

    return hadPreviouslyVoted;
}

function updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue, cancelVote) {
    let newScore = 0;
    if (cancelVote) {
        newScore = (voteMode === "upvote" || voteMode === "thread-upvote") ?
            currentScore - voteValue :
            currentScore + voteValue;
    } else {
        newScore = (voteMode === "upvote" || voteMode === "thread-upvote") ?
            currentScore + voteValue :
            currentScore - voteValue;
    }

    scoreDisplay.html(newScore);
}

function highlightMessageVoteArrow(id, voteMode) {
    $("#" + id).find("." + voteMode).addClass("voted");
}

function highlightOPVoteArrow(voteMode) {
    $("#original-post").find("." + voteMode).addClass("voted");
}
