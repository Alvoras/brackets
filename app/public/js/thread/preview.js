function updateHighlight() {
    $('pre code').each(function(i, block) {
        hljs.highlightBlock(block);
    });
}

function clearErrorMessages() {
    let commentErrorText = document.getElementById('comment-error-text');
    let commErTxtClasses = commentErrorText.getAttribute('class');

    if (commErTxtClasses.indexOf('display-error-text' === -1)) {
        commentErrorText.className = 'error-text';
    }
}

function mdToHtml(text, sanitize) {
    let params = {
        html: true,
        breaks: false,
        typographer: true
    };
    let md = window.markdownit(params);

    let htmlText = parseHTML(text, sanitize);

    let mdContent = md.render(htmlText);

    return mdContent;
}

function updatePreview(textContainer, sanitize) {
    let preview = document.getElementById('preview');

    if (!(preview)) {
        console.log("Canceling updatePreview -- Preview undefined");
        return;
    }

    let text = textContainer.val();


    let mdText = mdToHtml(text, sanitize);

    preview.innerHTML = mdText;


    if (preview.innerText === "") {
        preview.innerHTML = '<p id="preview-text"><span id="preview-placeholder">Vide</span></p>';
        return;
    }

    updateHighlight();
}

function parseHTMLColor(text) {
    let rgx = /!!(\w+|#[\w\d]{6}|#[\w\d]{3})(.*)_!/g;

    let htmlText = text.replace(rgx, "<span style=\"color: $1\">$2</span>");

    return htmlText;
}

function parseHTMLCenter(text) {
    let rgx = /!>(.*)<!/g;

    let htmlText = text.replace(rgx, "<center>$1</center>");

    return htmlText;
}

function parseMention(text) {
    let rgx = /(@\w*)/g;

    let htmlText = text.replace(rgx, "<span style=\"background: rgba(255, 213, 99, 0.8)\">$1</span>");

    return htmlText;
}

function parseHTML(text, sanitize) {
    /*let saneHtml = window.sanitizeHtml(text, {
        // Add img to the whitelist
      allowedTags: sanitizeHtml.defaults.allowedTags.concat([ 'img' ])
    });*/
    let html = text;

    if ((sanitize)) {
        html = sanitizeStyle(text);
    }

    let htmlColorText = parseHTMLColor(html);
    let htmlCenterText = parseHTMLCenter(htmlColorText);
    let htmlWithMention = parseMention(htmlCenterText);

    return htmlWithMention;
}

function sanitizeStyle(text) {
    let rgx = /<style>.*?<\/style>/g;

    let noStyleHtml = text.replace(rgx, "");

    return noStyleHtml;
}

function displayEmptyWarning() {
    $("#comment-error-text").addClass("display-error-text");
}
