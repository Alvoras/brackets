$(document).ready( () => {
    $(".tab-head-label").click( (e) => {
        // Prevent the by default pageOut() on a.click
        pageIn();
        $(".tab").css("display", "none");
        $("li").removeClass("active-tab");

        let target = $(e.target);

        let tabId = target.closest("a").attr("href");
        if ( !(tabId) ) {
            tabId = target.find("a").attr("href");
        }
        console.log($(tabId));
        $(tabId).css("display", "table");
        target.closest("li").addClass("active-tab");
    });

    $("body").click( (e) => {
        let target = $(e.target);
        if (target.hasClass("options-follow-icon")) {
            console.log("clicked");
            let threadId = target.closest(".card-container").attr('id');

            socket.emit('addSubscribe', threadId);
        }else if (target.hasClass("options-star-icon")) {
            let threadId = target.closest(".card-container").attr('id');

            socket.emit('addFavorite', threadId);
        }else if (target.hasClass('upvote')) {
          let threadId = target.closest(".card-container").attr('id');
          let voteMode = "upvote";
          let hadPreviouslyVoted = getCurrentVote(threadId);

          socket.emit('voteThread', threadId, voteMode, hadPreviouslyVoted);
        }else if (target.hasClass('downvote')) {
          let threadId = target.closest(".card-container").attr('id');
          let voteMode = "downvote";
          let hadPreviouslyVoted = getCurrentVote(threadId);

          socket.emit('voteThread', threadId, voteMode, hadPreviouslyVoted);
      }
    });

    $(".role-container").click( function(e) {
        let roleId = $(this).attr("id");
        console.log(roleId);
        socket.emit('selectRole', roleId);
    });

    // VOTES
    socket.on('voteThreadDone', (threadId, newVote, isBroadcast, hadPreviouslyVoted) => {
      console.log(threadId);
      let voteMode = (newVote === 1)?"upvote":"downvote";
      let voteIcons = $("#"+threadId+" .material-icons");
      let scoreDisplay = $("#"+threadId+" .score-display");
      let currentScore = parseInt(scoreDisplay.html());
      // Default values
      let voteValue = 1;
      let cancelVote = false;

      if ( !(isBroadcast) ) {
        console.log("not broadcast");
        if (voteIcons.hasClass("voted")){
            let currentVote = ( ($("#"+threadId+" .upvote").hasClass("voted")) )?"upvote":"downvote";
            voteIcons.removeClass("voted");


            if (currentVote !== voteMode) {
                highlightMessageVoteArrow(threadId, voteMode);
                // If the user changes his mind, we need to compensate
                // for the previous vote he made
                voteValue = 2;
            }else {
                // Tells the function to substract instead of add and vice versa
                cancelVote = true;
            }

            updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue, cancelVote);
        }else {
            highlightMessageVoteArrow(threadId, voteMode);
            updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue);
        }
      }else {
        console.log("broadcast");
        if ( (hadPreviouslyVoted.isTrue) ){
            if (hadPreviouslyVoted.previousVote !== voteMode) {
                // If the user changes his mind, we need to compensate
                // for the previous vote he made
                voteValue = 2;
            }else {
                // Tells the function to substract instead of add and vice versa
                cancelVote = true;
            }

            updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue, cancelVote);
        }else {
            updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue);
        }
    }
  });

    socket.on('favUpdated', (threadId) => {
        let starIcon = $("#"+threadId+" .options-star-icon").html().trim();

        if (starIcon === "star") {
            $("#"+threadId+" .options-star-icon").html("star_border");
            Materialize.toast('Favoris retiré', 2000);
            $(".toast").css('background', '#7FC468');
        }else {
            $("#"+threadId+" .options-star-icon").html("star");
            Materialize.toast('Favoris ajouté', 2000);
            $(".toast").css('background', '#7FC468');
        }
    });

    socket.on('subUpdated', (threadId) => {
        let followIcon = $("#"+threadId+" .options-follow-icon").html().trim();
        console.log(followIcon);
        console.log(threadId);

        if (followIcon === "turned_in") {
            $("#"+threadId+" .options-follow-icon").html("turned_in_not");
            Materialize.toast('Abonnement retiré', 2000);
            $(".toast").css('background', '#7FC468');
        }else {
            $("#"+threadId+" .options-follow-icon").html("turned_in");
            Materialize.toast('Abonnement ajouté', 2000);
            $(".toast").css('background', '#7FC468');
        }
    });

    socket.on('roleChanged', (roleData) => {
      $(".member-icon").attr("src", roleData.icon);
      $(".member-role").html(roleData.title);
      $(".member-role").attr("class", "member-role rarity-"+roleData.rarity);

      Materialize.toast('Distinction mise à jour', 2000);
      $(".toast").css('background', '#7FC468');
    });
});
