function initLS() {
    localStorage.setItem("collapseRulesContainer", false);
}

function handleLocalSession() {
    let ls = localStorage;

    if (ls.getItem("collapseRulesContainer") === null) {
        initLS();
    } else {
        mustCollapse = ls.getItem("collapseRulesContainer");
        if ((mustCollapse === "false")) {
            $("#rules-container").toggleClass("rules-expand");
            $("#rules-container-arrow").toggleClass("rotate-arrow-left");
        }
    }
}

function toggleLSRulesCollapse() {
    let ls = localStorage;
    let currentState = ls.getItem("collapseRulesContainer");

    // Local storage only store strings and "== true" appears to not be working
    let newState = (currentState === "true") ? false : true;

    ls.setItem("collapseRulesContainer", newState);
}
