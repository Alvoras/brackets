function toggleNewThreadForm() {
    $("#grey-overlay").toggleClass("display");
    $("#new-thread-container").toggleClass("display-new-thread-form");
}
function displayNewReply() {
    $(".post-container").last().fadeIn();
}
function animateNewThread() {
    let newCard = $(".card-container").first();

    setTimeout( () => {
        newCard.removeClass("new-card");
        setTimeout( () => {
            newCard.removeClass("noisy");
        }, 1000);
        setTimeout( () => {
            $(".glitch").removeClass("glitch");
        }, 6000);
        $(".card-content").addClass("flash");
    }, 100)
}
function animateNewReply() {
    let newPost = $(".post-container").last();
    setTimeout( () => {
        newPost.removeClass("new-card");
    }, 100)
}
function pageOut(link) {
    $("#main-container").addClass('page-out');

    //window.location(link);
}
function pageIn() {
    $("#main-container").removeClass('page-out');
}
function toggleThreadForm() {
    let newThreadContainer = $("#new-thread-container");
    newThreadContainer.toggleClass('page-out');
    newThreadContainer.toggleClass('z-index-999');
    $("header").toggleClass('page-out');
    $("#add-button").toggleClass('page-out');
}
function displayNotificationPanel(e){
    console.log("display notif panel");
    let notifPanel = $("#notifications-panel-container");
    let greyOverlay = $("#grey-overlay");
    let rulesContainer = $("#rules-container");

    if ($(e.target).attr("id") === "grey-overlay") {
        if ( !(notifPanel.hasClass("display-notifications-panel")) ) { return };
    }

    notifPanel.toggleClass("display-notifications-panel");
    $("body").toggleClass("notifications-panel-offset");
    $("#main-container").toggleClass("notifications-panel-offset");
    $("header").toggleClass("notifications-panel-offset");

    if (rulesContainer.length > 0) {
        if ( !(rulesContainer.hasClass("rules-expand")) ) {
            rulesContainer.toggleClass("rules-container-notification-collapse");
        }else {
            rulesContainer.toggleClass("notifications-panel-offset");
        }
    }
    greyOverlay.toggleClass("display");
    greyOverlay.toggleClass("notifications-panel-offset");

    if ( !(notifPanel.hasClass("display-notifications-panel")) ) {
        setTimeout( () => {
            $("#add-button").fadeToggle(200);
        }, 200);
    }else {
        $("#add-button").fadeToggle(100);
    }

    $("#clear-all-container").toggleClass("display-clear-all");
}
function glitch(blueEl, redEl, size) {
    let beforeLoopDuration = 3*1000; // 3 sec
    let afterLoopDuration = 2.5*1000; // 2.5 sec
    let steps = 30;

    let i = 0;
    let counterAfter = 0;
    let counterBefore = 0;
    let tab01 = [];
    let tab02 = [];
    let tab11 = [];
    let tab12 = [];

    for (i = 0; i < 20; i++) {
        tab01[i] = Math.floor((Math.random(100)*300*size)+1);
        tab02[i] = Math.floor((Math.random(100)*300*size)+1);
    }

    for (i = 0; i < 20; i++) {
        tab11[i] = Math.floor((Math.random(100)*300*size)+1);
        tab12[i] = Math.floor((Math.random(100)*300*size)+1);
    }

    //setTimeout( () => {
        setInterval( () => {
                blueEl.css("clip", "rect("+tab01[counterBefore%19]+"px 9999px "+tab02[counterBefore%19]+"px 0)");
                tab01[(counterBefore%19)+1] = Math.floor((Math.random(100)*300*size)+1);
                tab02[(counterBefore%19)+1] = Math.floor((Math.random(100)*300*size)+1);
                counterBefore++;
        }, beforeLoopDuration/30); // <=> 5% step keyframe
    //}, 3500);

    //setTimeout( () => {
        setInterval( () => {
                redEl.css("clip", "rect("+tab11[counterAfter%19]+"px 9999px "+tab12[counterAfter%19]+"px 0)");
                tab11[(counterAfter%19)+1] = Math.floor((Math.random(100)*300*size)+1);
                tab12[(counterAfter%19)+1] = Math.floor((Math.random(100)*300*size)+1);
                counterAfter++;
        }, afterLoopDuration/30); // <=> 5% step keyframe
    //}, 3500);
}
function signalNewNotification() {
    $("#notification-icon").addClass('new-notification-dot');
}
function removeNewNotificationSignal() {
    $("#notification-icon").removeClass('new-notification-dot');
}
function removeAllNotifications() {
    let notificationCard = $(".notification-card");
    notificationCard.addClass("noisy");
    notificationCard.find(".notification-content-container").addClass("opacity-0");
    setTimeout( () => {
        notificationCard.remove();
    },300);
}
function removeSingleNotification(notification) {
    notification.addClass("noisy");
    notification.find(".notification-content-container").addClass("opacity-0");
    setTimeout( () => {
        notification.remove();
    },300);
}

/*
function toggleAddNewThread() {
    $("#main-container").toggleClass("reduce-fade-out");
    $("#new-thread-container").toggleClass("spawn-new-thread");
}*/
/*
function newNotification(text, type) {
   let notificationContainer = $("#notification-container");

   if ( notificationContainer.hasClass("displayed") ) {
      notificationContainer.removeClass("displayed");
      notificationContainer.css("opacity", "0"); // REMOVE PREVIOUS NOTIFICATION
      notificationContainer.css("transform", "translateX(320px)");

      setTimeout(function () {
         notificationContainer.css("opacity", "1"); // RESTORE OPACITY
         displayNotification(text, type);
      }, 500);
   }else {
      displayNotification(text, type);
   }

}
function displayNotification(text, type) {
   let notificationContainer = $("#notification-container");

   notificationContainer.addClass("displayed");

   $("#notification-text").html(text);

   if (type === "success") {
      var icon = "/images/icons8-Checked-50.png";
      notificationContainer.css("background", "#066d1d");
      $("#notification-text-container").css("background", "#0b8c28");
   }else if (type === "fail" ||type === "no-results") {
      var icon = (type === "fail")?"/images/icons8-Cancel-50.png":"/images/icons8-Dislike-50.png";
      notificationContainer.css("background", "#7a0906");
      $("#notification-text-container").css("background", "#a3100b");
   }

   $("#notification-icon").attr("src", icon);

   notificationContainer.css("transform", "translateX(-320px)");
   setTimeout(function () {
      notificationContainer.css("opacity", "0");
      setTimeout(function () {
         notificationContainer.css("transform", "translateX(320px)");
         notificationContainer.removeClass("displayed");
         setTimeout(function () {
            notificationContainer.css("opacity", "1");
         }, 500);
      }, 600);
   }, 3000);

}
*/
