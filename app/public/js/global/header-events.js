$(document).ready(() =>  {
    $("#search-bar").keyup((e) => {
        if (e.keyCode === 13) {
            window.location.pathname = "/s/" + $("#search-bar").val();
        }
    });

    // NOTIFICATIONS
    // newNotificationCreated is emited from addNotification in db.js
    socket.on('newNotificationCreated', (values) => {
        socket.emit('isNotificationClient', values);
    });

    socket.on('receiveNotification', (values) => {
        signalNewNotification();
        appendNewNotification(values);

        Materialize.toast('<i class="material-icons toast-notification-icon">notifications</i> Nouvelle notification', 2000);
        $(".toast").css('background', '#7FC468');
    });

    $("#notification-back").click((e) => {
        displayNotificationPanel(e);
        $("#notification-icon").removeClass('new-notification-dot');
    });

    $("body").click((e) => {
        let target = $(e.target);
        if (target.hasClass("notification-close-icon")) {
            pageIn();
            e.preventDefault();

            let notificationCard = target.closest('.notification-card');

            removeSingleNotification(notificationCard);
        }
    });

    $("#notification-icon, #grey-overlay").click((e) => {
        let removeNotifications = false;
        let notificationIds = getNotificationIds();

        displayNotificationPanel(e);
        $("#notification-icon").removeClass('new-notification-dot');
        socket.emit('markNotificationsAsRead', notificationIds, removeNotifications);
    });

    $("#clear-all-container").click(() => {
        let removeNotifications = true;
        let notificationIds = getNotificationIds();

        $("#notification-icon").removeClass('new-notification-dot');
        socket.emit('markNotificationsAsRead', notificationIds, removeNotifications);
    });

    socket.on('notificationsHasBeenRead', () => {
        removeAllNotifications();
    });
});
