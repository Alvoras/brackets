$(document).ready( () => {
    pageIn();

    socket.on('nextPageLoaded', (dataLength, threadData, newPageNumber, isMod) => {
        let i = 0;
        appendPageAnchor(newPageNumber);

        for (i = 0; i < dataLength; i++) {
            if ( ($("#"+threadData[i].id).length === 0)) {
                appendLoadedThread(threadData[i], isMod, newPageNumber)
            }
        }
        appendScrollTrigger(newPageNumber);
    });

    socket.on('redirect', (page) => {
        window.location.href = page;
    });

    socket.on('lightNotif', (text) => {
        Materialize.toast(text, 4000);
        $(".toast").css('background', 'red');
    });
    // OPTIONS DROPDOWN
    $("#options-button-container").click( () => {
        $("#dropdown-options-container").toggleClass('display');
    });

    $("a").click( function() {
        if ($(this).hasClass("no-page-out")) {
            return;
        }
        pageOut();
    });

    // Ugly fix
    // Prevent the current sort mode indicator to do anything if clicked on
    $(".selected-sort-mode").click( (e) => {
        e.preventDefault();
        pageIn();
    });
    /*$(".no-page-out").click( () => {
        pageIn();
    });*/
});
