function appendReply(threadId, lastId, userData, text, isBroadcast, currentUser) {
    let postContainerClass = (userData.is_admin)?"post-container highlight-admin-post":"post-container";
    let editButton = '';
    let chooseReplyButton = '';

    if ( !(isBroadcast) || (userData.is_mod) ) {
        editButton = '<div class="edit-button-container">\
                        <p class="edit-button">Edit</p>\
                      </div>';
    }

    // We could retrieve the current client user from the profile icon href, but that would enable
    // nasty html modification attacks
    // See below

    /*
    if (currentUser ===  opName) {
        chooseReplyButton = '<div class="valid-button-container">\
                           <button type="button" class="valid-reply-button" name="valid-reply-button">Choisir cette réponse</button>\
                       </div>'
    }*/

    let newPost = '<div class="'+postContainerClass+' new-card" id="'+lastId+'">\
        <div class="bottom-container">\
          <div class="identity">\
                  <p class="member-name">'+userData.pseudo+'</p>\
              <div class="member-subtitle-container">\
                      <img src="'+userData.icon+'" class="member-icon">\
                      <p class="member-role">'+userData.role_title+'</p>\
              </div>\
              <div class="member-class-container flex-center">\
                  <p class="member-class-name">'+userData.className+'</p>\
              </div>\
            <p class="member-stats">'+userData.score+' points</p>\
          </div>\
          <div class="post-stats">\
            <div class="created-container">\
              <p class="created">Créé maintenant</p>\
            </div>\
          </div>\
        </div>\
        <div class="post-response-container">\
            <div class="vote-container flex-center">\
                <i class="material-icons upvote">keyboard_arrow_up</i>\
                <p class="score-display">0</p>\
                <i class="material-icons downvote">keyboard_arrow_down</i>\
            </div>\
            <div class="post-response">'+text+'</div>\
        </div>\
        <div class="bottom-reply-container">\
            '+editButton+'\
            '+chooseReplyButton+'\
        </div>\
    </div>';

    $(".main-posts-container").append(newPost);

    animateNewReply();
}
function appendNewThread(threadData, isMod) {
    let modTools = '';

    if ( (isMod) ) {
        modTools = '<i class="material-icons mod-tool-icon delete-thread-icon">delete_forever</i>\
                        <i class="material-icons mod-tool-icon lock-thread-icon">lock</i>\
                   ';

    }

    let newCard = '<div id="'+threadData.id+'" class="card-container flex-center noisy new-card">\
        <div class="category-container flex-center">\
            '+modTools+'\
        </div>\
        <div class="card-content">\
            <div class="card-body-container flex-center">\
                    <div class="aligned-container">\
                        <div class="vote-container flex-center">\
                        <i class="material-icons upvote">keyboard_arrow_up</i>\
                        <p class="score-display">0</p>\
                        <i class="material-icons downvote">keyboard_arrow_down</i>\
                        </div>\
                    </div>\
                    <div class="current-state-container flex-center">\
                        <div class="current-state-answers-container">\
                            <p class="current-state-answers flex-center-column"><b>0</b> réponses</p>\
                        </div>\
                        <div class="separator"></div>\
                        <div class="current-state-vote-container">\
                            <p class="current-state-vote flex-center-column"><b>0</b> votes</p>\
                        </div>\
                    </div>\
            <div class="top-card-container flex-center">\
                <div class="title-container">\
                    <a href="/thread/'+threadData.id+'/0" class="title ">'+threadData.title+'</a>\
                    <div class="category-link-container">\
                        <a class="category-link" href="/c/'+threadData.category+'/all/0">'+threadData.category+'</a>\
                    </div>\
                </div>\
            </div>\
            <div class="aligned-container flex-center">\
                <div class="options-container flex-center">\
                    <div class="options-icon-container flex-center align-subicon">\
                        <i class="material-icons options-add-icon" data-text="add">add</i>\
                        <i class="material-icons options-follow-icon">turned_in_not</i>\
                    </div>\
                    <div class="options-icon-container flex-center">\
                        <i class="material-icons options-star-icon">star_border</i>\
                    </div>\
                </div>\
            </div>\
        </div>\
    </div>\
</div>';

    $(newCard).insertAfter("#page-0 .card-list-header");

    animateNewThread();
}
function appendLoadedThread(threadData, isMod, newPageNumber) {
    let modTools = '';
    let validatedLabel = '';
    let closedIcon = '';
    let closedClass = '';
    let upvoted = '';
    let downvote = '';
    let subIcon = "turned_in_not";
    let favIcon = "star_border";

    if ( (isMod) ) {
        modTools = '<i class="material-icons mod-tool-icon delete-thread-icon">delete_forever</i>\
                        <i class="material-icons mod-tool-icon lock-thread-icon">lock</i>\
                   ';

    }
    if ( (threadData.is_validated) ) {
        validatedLabel = '<div class="validated-label-container flex-center">\
                            <div class="validated-label-icon">\
                                <i class="material-icons">done_all</i>\
                            </div>\
                        </div>';
    }
    if ( (threadData.is_closed) ) {
        closedIcon = '<div class="lock-label-container flex-center">\
                        <div class="lock-label-icon">\
                            <i class="material-icons">lock</i>\
                        </div>\
                    </div>';
        closedClass = 'locked';
    }
    if (threadData.voteTypeThread === 'upvote'){
        upvoted = 'voted'
    };

    if (threadData.voteTypeThread === 'downvote'){
        upvoted = 'voted'
    };

    if ( (threadData.isSub) ) {
        subIcon = "turned_in";
    }

    if ( (threadData.isFav) ) {
        favIcon = "star";
    }

    /*let newCard = '<div id="'+threadData.id+'" class="card-container flex-center '+closedClass+'">\
    <div class="category-container flex-center">\
        '+modTools
        +validatedLabel
        +closedIcon+'\
    </div>\
    <div class="card-body-container">\
        <div class="vote-container flex-center">\
            <i class="material-icons upvote '+upvoted+'">keyboard_arrow_up</i>\
            <p class="score-display">'+threadData.score+'</p>\
            <i class="material-icons downvote '+downvote+'">keyboard_arrow_down</i>\
        </div>\
        <div class="top-card-container flex-center">\
            <div class="title-container">\
                <a href="/thread/'+threadData.id+'" class="title">'+threadData.title+'</a>\
            </div>\
        </div>\
        <div class="current-state-container flex-center">\
            <div class="current-state-answers-container flex-center">\
                <p class="current-state-answers">'+threadData.answerCount+'</p>\
                <p>réponses</p>\
            </div>\
            <div class="separator"></div>\
            <div class="current-state-vote-container flex-center">\
                <p class="current-state-vote">'+threadData.answerCount+'</p>\
                <p>votes</p>\
            </div>\
        </div>\
        <div class="options-container flex-center">\
            <div class="options-icon-container flex-center">\
                <i class="material-icons options-add-icon">add</i>\
                <i class="material-icons options-follow-icon">'+subIcon+'</i>\
            </div>\
            <div class="options-icon-container flex-center">\
                <i class="material-icons options-star-icon">'+favIcon+'</i>\
            </div>\
    </div>';*/
    console.log(threadData);

    let newCard = '<div id="'+threadData.id+'" class="card-container flex-center '+closedClass+'">\
        <div class="category-container flex-center">\
            '+modTools
            +validatedLabel
            +closedIcon+'\
        </div>\
        <div class="card-body-container flex-center">\
                <div class="aligned-container">\
                    <div class="vote-container flex-center">\
                    <i class="material-icons upvote '+upvoted+'">keyboard_arrow_up</i>\
                    <p class="score-display">'+threadData.score+'</p>\
                    <i class="material-icons downvote '+downvote+'">keyboard_arrow_down</i>\
                    </div>\
                </div>\
                <div class="current-state-container flex-center">\
                    <div class="current-state-answers-container">\
                        <p class="current-state-answers flex-center-column"><b>'+threadData.answerCount+'</b> réponses</p>\
                    </div>\
                    <div class="separator"></div>\
                    <div class="current-state-vote-container">\
                        <p class="current-state-vote flex-center-column"><b>'+threadData.voteCount+'</b> votes</p>\
                    </div>\
                </div>\
        <div class="top-card-container flex-center">\
            <div class="title-container">\
                <a href="/thread/'+threadData.id+'/0" class="title">'+threadData.title+'</a>\
                <div class="category-link-container">\
                    <a class="category-link" href="/c/'+threadData.category+'/all/0">'+threadData.category+'</a>\
                </div>\
            </div>\
        </div>\
        <div class="aligned-container flex-center">\
            <div class="options-container flex-center">\
                <div class="options-icon-container flex-center align-subicon">\
                    <i class="material-icons options-add-icon">add</i>\
                    <i class="material-icons options-follow-icon">'+subIcon+'</i>\
                </div>\
                <div class="options-icon-container flex-center">\
                    <i class="material-icons options-star-icon">'+favIcon+'</i>\
                </div>\
            </div>\
        </div>\
    </div>\
</div>';



    //$("#card-wrapper").append(newCard);
    $("#page-"+newPageNumber).append(newCard);

    //animateNewThread();
}
function appendPageAnchor(newPageNumber) {
    let pageAnchor = '<div id="page-'+newPageNumber+'" class="page-anchor column-5-layout loaded-page-anchor">\
    <p class="page-anchor-title flex-center">Page '+(newPageNumber+1)+'</p>\
    </div>';
    $("#card-wrapper").append(pageAnchor);
}
function appendScrollTrigger(newPageNumber) {
    let scrollTrigger = '<div id="scroll-trigger-'+(newPageNumber+1)+'" class="scroll-trigger"></div>';

    $("#page-"+newPageNumber).append(scrollTrigger);
}
function isVisible(elem) {
    // https://stackoverflow.com/questions/487073/check-if-element-is-visible-after-scrolling
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = elem.offset().top;
    var elemBottom = elemTop + elem.height();

    return ((elemBottom < docViewBottom) && (elemTop > docViewTop));
}
function appendNewNotification(values) {
    $("#no-notifications-container").remove();
    let notificationColor = "green";

    if (values.type === "mention") {
        notificationColor = "#F2E200";
    }else if (values.type === "mention") {
        notificationColor = "rgba(252, 156, 13, 0.78)";
    }
    let newNotificationCard = '\
             <a href="'+values.href+'">\
               <div id="'+values.lastNotificationId+'" class="notification-card">\
                   <div class="notification-content-container">\
                       <div class="notification-color" style="background: '+notificationColor+'"></div>\
                       <div class="notification-top-container flex-center">\
                           <div class="notification-date-container">\
                               <p class="notification-date">Maintenant</p>\
                           </div>\
                           <div class="notification-close-icon-container">\
                               <i class="material-icons close-icon notification-close-icon">close</i>\
                           </div>\
                       </div>\
                       <p class="notification-text">'+values.content+'</p>\
                   </div>\
               </div>\
             </a>';

    $("#notifications-panel").append(newNotificationCard);
}
function getNotificationIds() {
    let notifications = $(".notification-card");
    let notificationIds = [];
    let i = 0;

    for (i = 0; i < notifications.length; i++) {
        notificationIds[i] = $(notifications[i]).attr('id');
    }

    return notificationIds;
}
