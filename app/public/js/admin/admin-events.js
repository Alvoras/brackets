$(document).ready( () => {
    socket.on('userAdded', () => {
        // Diplay notification
        Materialize.toast('User added successfully', 4000);
        $(".toast").css('background', '#7FC468');    });

    $("#new-user-button").click( (e) => {
        e.preventDefault();

        let values = {};

        values.pseudo = $("#pseudo").val();
        values.name = $("#name").val();
        values.surname = $("#surname").val();
        values.email = $("#email").val();
        values.class_id = $("#classe").val();

        if ( !(checkValues(values)) ) {
            // TODO add warning
            console.log("Empty value found");
        }

        console.log(values);
        socket.emit('addNewUser', values);
    });

    $("#bulk-new-user-button").click( (e) => {
        e.preventDefault();

        let bulkInput = $("#bulk-new-user-input").val();
        let parsedInput = bulkInput.split(',');

        let values = [];
        let line;
        console.log("PARSED INPUT", parsedInput);
        for (var i = 0; i < parsedInput.length; i++) {
            values[i] = {};
            line = parsedInput[i].split(':');

            values[i].pseudo = line[0];
            values[i].email = line[1];
            values[i].class_id = parseInt(line[2]);

            if ( !(checkValues(values)) ) {
                // TODO add warning
                console.log("Empty value found");
            }

            socket.emit('addNewUser', values[i]);
        }
    });
    $(".mod-action-button").click( function () {
        let clicked  = $(this);
        let mode = '';

        if ( (clicked.hasClass("ban-button")) ) {
            mode = "ban";
        }else if ( (clicked.hasClass("delete-button")) ) {
            mode = "delete";
        }else if ( (clicked.hasClass("reset-button")) ) {
            mode = "reset";
        }

        let currentLine = clicked.parent().parent();
        let userId = currentLine.find(".id-cell").html();
        let currentState = currentLine.find(".is-"+mode+"-cell").html();

        let params = {
            mode,
            userId,
            currentState
        };

        socket.emit("adminAction", params);
    });
    socket.on('adminActionDone', (params) => {
        if (params.mode === "ban") {
            let newStateText = (params.newState === 1)?"Débannir":"Bannir";
            console.log(params.newState, newStateText);
            $("#"+params.userId).find('.is-ban-cell').html(params.newState);
            $("#"+params.userId).find('.ban-button').html(newStateText);

            Materialize.toast('User '+params.userId+' is_banned = '+params.newState, 2000);
            $(".toast").css('background', '#7FC468');
        }else if (params.mode === "delete") {
            let newStateText = (params.newState === 1)?"Restorer":"Supprimer";

            $("#"+params.userId).find('.is-delete-cell').html(params.newState);
            $("#"+params.userId).find('.delete-button').html(newStateText);

            Materialize.toast('User '+params.userId+' is_deleted = '+params.newState, 2000);
            $(".toast").css('background', '#7FC468');
        }else if (params.mode === "reset") {
            let newStateText = (params.newState === 1)?"Activer":"Reset";

            $("#"+params.userId).find('.is-reset-cell').html(params.newState);
            $("#"+params.userId).find('.reset-button').html(newStateText);

            Materialize.toast('User '+params.userId+' first_time = '+params.newState, 2000);
            $(".toast").css('background', '#7FC468');
        }
    });
});

function checkValues(values) {
    let result = true;
    $.each(values, (i, val) => {
        console.log(i, val);
        if (val === undefined || val === '') {
            // display error message
            result = false;
        }
    });
    return result;
}
