$(document).ready(() => {
    let editBuffer = '';

    updatePreview($("#new-thread-textarea"));

    // if collapseRulesContainer undefined, then set it to false
    // if collapseRulesContainer : true found in local storage, collapse it
    handleLocalSession();

    // PASSWORD DEFINE FORM
    $("#define-password-button").click(() => {
        $(".error-text").removeClass("display-error-text");

        let formData = {};
        formData.password = $("#define-password-pwd").val();
        formData.confirmPwd = $("#define-password-confirm-pwd").val();
        formData.token = $("#define-password-token").val();
        formData.cgu = document.getElementById('cgu-checkbox').checked;

        let errors = checkDefinePasswordForm(formData);

        if (errors.length !== 0) {
            for (var i = 0; i < errors.length; i++) {
                $("#error-text-" + errors[i]).addClass("display-error-text");
            }
        } else {
            socket.emit('definePassword', formData);
        }
    });

    socket.on('definePasswordDone', (success) => {
        if ((success)) {
            Materialize.toast('Mot de passe bien mis à jour', 2000);
            $(".toast").css('background', '#7FC468');
        } else {
            Materialize.toast('Echec de la mise à jour du mot de passe', 2000);
            $(".toast").css('background', 'hsl(3, 81.8%, 49.6%)');
        }

        setTimeout(() => {
            window.location = ('/');
        }, 2000);
    });

    $("body").keyup(function(e) {
        switch (e.keyCode) {
            case 27:
                if (!($("#new-thread-container").hasClass("page-out"))) {
                    pageIn();
                    toggleThreadForm();
                }
                break;
            default:
                break;
        }
    });

    $("#refresh-preview").click(() => {
        $(".error-text").removeClass("display-error-text");

        let sanitize = true;
        updatePreview($("#new-thread-textarea"), sanitize);
    });

    /*$(window).scroll(() => {
        console.log("scroll");
        if ((isVisible($(".scroll-trigger").last()))) {
            let triggerId = $(".scroll-trigger").last().attr("id");
            let pageNumber = triggerId.split('-')[2] * 1; // *1 : convert string into number

            let lastPage = $(".page-anchor").last().attr("id")
            let lastPageLoaded = lastPage.split('-')[2] * 1;

            let windowPathArray = location.pathname.split('/');
            let sortMode = windowPathArray[windowPathArray.length - 1];

            if (pageNumber !== lastPageLoaded) {
                socket.emit('loadNextPage', pageNumber, sortMode);
            }
        }
    });*/

    $("body").click((e) => {
        target = $(e.target);

        if (target.hasClass("options-follow-icon")) {
            let threadId = target.closest(".card-container").attr('id');

            socket.emit('addSubscribe', threadId);
        } else if (target.hasClass("options-star-icon")) {
            let threadId = target.closest(".card-container").attr('id');

            socket.emit('addFavorite', threadId);
        } else if (target.hasClass('upvote')) {
            let threadId = target.closest(".card-container").attr('id');
            let voteMode = "upvote";
            let hadPreviouslyVoted = getCurrentVote(threadId);

            socket.emit('voteThread', threadId, voteMode, hadPreviouslyVoted);
        } else if (target.hasClass('downvote')) {
            let threadId = target.closest(".card-container").attr('id');
            let voteMode = "downvote";
            let hadPreviouslyVoted = getCurrentVote(threadId);

            socket.emit('voteThread', threadId, voteMode, hadPreviouslyVoted);
        } else if (target.hasClass('delete-thread-icon')) {
            let threadId = target.closest(".card-container").attr('id');
            let values = {
                is_deleted: 1
            };

            socket.emit('updateFrontThread', values, threadId);
        } else if (target.hasClass('lock-thread-icon')) {
            let threadId = target.closest(".card-container").attr('id');
            let values = {};

            if ($("#" + threadId).hasClass("locked")) {
                values = {
                    is_closed: 0
                };
            } else {
                values = {
                    is_closed: 1
                };
            }

            socket.emit('updateFrontThread', values, threadId);
        }
        /*else if (target.hasClass("title")) {
                  let titleContent = target.html();
                  editBuffer = titleContent;
                  target.replaceWith('<textarea class="edit-title-textarea">'+replyContent+'</textarea>');
                }*/
    });

    /*  $(".title").keyup( (e) => {
      console.log(e.target);
      if (e.keyCode === 13) { // ENTER

        socket.emit('updateTitle', values, threadId);
      }
    });
*/
    socket.on('frontThreadUpdated', (threadId, values) => {
        let threadContainer = $("#" + threadId);

        if ((values.is_deleted)) {
            threadContainer.remove();

        } else if (values.is_closed !== undefined) {
            if ((values.is_closed)) {
                threadContainer.addClass("locked");
                let lockFlagIcon = '<div class="lock-label-container flex-center">\
                                  <div class="lock-label-icon">\
                                      <i class="material-icons">lock</i>\
                                  </div>\
                              </div>';
                threadContainer.append(lockFlagIcon);
            } else if (!(values.is_closed)) {
                threadContainer.removeClass("locked");
                let lockFlagIcon = threadContainer.find(".lock-label-container").remove();
            }
        }
    });

    /*$("#new-thread-textarea").keyup((e) => {
        updatePreview($("#new-thread-textarea"));
    });*/

    // VOTES
    socket.on('voteThreadDone', (threadId, newVote, isBroadcast, hadPreviouslyVoted) => {
        let voteMode = (newVote === 1) ? "upvote" : "downvote";
        let voteIcons = $("#" + threadId + " .material-icons");
        let scoreDisplay = $("#" + threadId + " .score-display");
        let currentScore = parseInt(scoreDisplay.html());
        // Default values
        let voteValue = 1;
        let cancelVote = false;

        if (!(isBroadcast)) {
            if (voteIcons.hasClass("voted")) {
                let currentVote = (($("#" + threadId + " .upvote").hasClass("voted"))) ? "upvote" : "downvote";
                voteIcons.removeClass("voted");


                if (currentVote !== voteMode) {
                    highlightMessageVoteArrow(threadId, voteMode);
                    // If the user changes his mind, we need to compensate
                    // for the previous vote he made
                    voteValue = 2;
                } else {
                    // Tells the function to substract instead of add and vice versa
                    cancelVote = true;
                }

                updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue, cancelVote);
            } else {
                highlightMessageVoteArrow(threadId, voteMode);
                updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue);
            }
        } else {
            if ((hadPreviouslyVoted.isTrue)) {
                if (hadPreviouslyVoted.previousVote !== voteMode) {
                    // If the user changes his mind, we need to compensate
                    // for the previous vote he made
                    voteValue = 2;
                } else {
                    // Tells the function to substract instead of add and vice versa
                    cancelVote = true;
                }

                updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue, cancelVote);
            } else {
                updateScoreDisplay(scoreDisplay, currentScore, voteMode, voteValue);
            }
        }
    });

    socket.on('favUpdated', (threadId) => {
        let starIcon = $("#" + threadId + " .options-star-icon");
        let starIconHtml = starIcon.html().trim();
        if (starIconHtml === "star") {
            starIcon.html("star_border");
            Materialize.toast('Favoris retiré', 2000);
            $(".toast").css('background', '#7FC468');
        } else {
            starIcon.html("star");
            Materialize.toast('Favoris ajouté', 2000);
            $(".toast").css('background', '#7FC468');
        }
    });

    socket.on('subUpdated', (threadId) => {
        let followIcon = $("#" + threadId + " .options-follow-icon");
        let followIconHtml = followIcon.html().trim();

        if (followIconHtml === "turned_in") {
            followIcon.html("turned_in_not");
            Materialize.toast('Abonnement retiré', 2000);
            $(".toast").css('background', '#7FC468');
        } else {
            followIcon.html("turned_in");
            Materialize.toast('Abonnement ajouté', 2000);
            $(".toast").css('background', '#7FC468');
        }

    });

    // NEW THREAD
    $("#new-thread-button").click((e) => {
        e.preventDefault();
        $(".error-text").removeClass("display-error-text");

        let threadData = {};
        let title = $("#new-thread-name-input").val();
        let params = {
            html: true,
            breaks: false,
            typographer: true
        };
        let md = window.markdownit(params);

        threadData.title = window.sanitizeHtml(title, {
            // Add img to the whitelist
            allowedTags: sanitizeHtml.defaults.allowedTags = []
        });

        //threadData.title = title;

        let rawDescription = $("#new-thread-textarea").val();
        /*threadData.description = window.sanitizeHtml(rawDescription, {
            // Add img to the whitelist
          allowedTags: sanitizeHtml.defaults.allowedTags.concat([ 'img' ])
      });*/
        threadData.description = parseHTML(rawDescription);
        /*let rawDescription = $("#new-thread-textarea").val();
        threadData.description = md.render(rawDescription);*/
        threadData.category = $("#categories-dropdown").val();

        // ADD FIELDS CHECK

        let errors = checkNewThreadForm(threadData);

        if (errors.length !== 0) {
            for (var i = 0; i < errors.length; i++) {
                $("#error-text-" + errors[i]).addClass("display-error-text");
            }
        } else {
            socket.emit("openNewThread", threadData);
        }
    });

    socket.on('newThreadOpened', (threadData, isMod, isSender) => {
        // BUILD AND DISPLAY NEW POST
        Materialize.toast('Nouveau post ouvert', 2000);
        $(".toast").css('background', '#7FC468');

        appendNewThread(threadData, isMod);

        if ((isSender)) {
            if (!($("#new-thread-container").hasClass("page-out"))) {
                pageIn();
                toggleThreadForm();
            }
        }
    });

    $("#rules-collapse-button-container").click(function() {
        $("#rules-container").toggleClass("rules-expand");
        $("#rules-container-arrow").toggleClass("rotate-arrow-left");

        // toggle local storage collapseRulesContainer rule
        toggleLSRulesCollapse();
    });

    $("#add-button").click((e) => {
        pageOut();
        toggleThreadForm();
        //toggleNewThreadForm();
    });

    $("#new-thread-close").click((e) => {
        pageIn();
        toggleThreadForm();
    });
});
