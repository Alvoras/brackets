function checkNewThreadForm(threadData) {
    let errors = [];

    if ( threadData.title === '' ) {
        errors[errors.length] = 1;
    }

    if ( threadData.description === '' ) {
        errors[errors.length] = 2;
    }

    if ( threadData.category === '' ) {
        errors[errors.length] = 3;
    }

    return errors;
}

function checkDefinePasswordForm(formData) {
    let errors = [];

    // Do not overlap define password form error messages numbers with new thread form error messages
    if ( !(formData.cgu) ) {
        errors[errors.length] = 4;
    }

    let re = /.*[0-9].*/;
    let pattern = new RegExp(re);

    if ( formData.password === '' || formData.password.length < 7 || !(pattern.test(formData.password))) {
        errors[errors.length] = 5;
    }

    if ( formData.confirmPwd === '' || formData.confirmPwd !== formData.password) {
        errors[errors.length] = 6;
    }

    return errors;
}
