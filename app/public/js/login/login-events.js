$(document).ready( () => {
    $("#error-container").addClass("display-error-container");

    socket.on('sendNewPasswordFailed', () => {
        Materialize.toast('Pseudo ou email invalide', 2000);
        $(".toast").css('background', 'hsl(3, 81.8%, 49.6%)');
    });

    socket.on('sendNewPasswordSuccess', () => {
        $(".login-form-input").val('');

        Materialize.toast('Nouveaux identifiants envoyés', 2000);
        $(".toast").css('background', '#7FC468');
    });

    $("#reset-form").submit(() => {
        return false;
    });

    $(".tab-name").click( function() {
        let that = $(this);
        let loginForm = $("#login-form-container");
        let resetForm = $("#reset-form-container");

        if (that.attr("id") === "tab-login" && !(loginForm.hasClass("hide-login-tab"))) {
            return;
        }else if (that.attr("id") === "tab-reset" && resetForm.hasClass("display-reset-tab")) {
            return;
        }

        loginForm.toggleClass("hide-login-tab");
        resetForm.toggleClass("display-reset-tab");
        $("#marker").toggleClass("highlight-tab");
    });

    $("#reset-button").click( () => {
        let userData = {};

        userData.pseudo = $("#reset-pseudo").val();
        userData.email = $("#reset-email").val();

        if (userData.pseudo.length === 0 || userData.email.length === 0) {
            // display error
            console.log("Cannot send empty forms");
            return;
        }

        socket.emit("sendNewPassword", userData);
    });

    /*let logoGlitchSize = 8;
    glitch($("#login-text-before"), $("#login-text-after"), logoGlitchSize);

    let subtextGlitchSize = 0.8;
    glitch($("#login-subtext-before"), $("#login-subtext-after"), subtextGlitchSize);
    */
});
