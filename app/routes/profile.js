const express = require('express');
const router = express.Router();

const fs = require('fs');
const db = require('../modules/db');
const forum = require('../modules/forum');
const userManagement = require('../modules/userManagement');
const connection = db.connect();
const cookieParser = require('cookie-parser');
const config = JSON.parse(fs.readFileSync(__dirname+'/../config/config.json'));
const categories = JSON.parse(fs.readFileSync(__dirname+'/../config/categories.json'));

router.use(cookieParser());

router.get('/:user', (req, res, next) => {
  let currentUser = req.params.user;

  if ( !(userManagement.authenticate(req, res, {user : currentUser})) ){
    res.redirect('/');
    return;
  }

  db.getUserId(connection, currentUser).then( (userId) => {
          db.getInfo(connection, "users", userId).then( (userData) => {
              db.getClass(connection, userId).then( (opClass) => {
                userData.className = opClass.name;
                userData.classIcon = opClass.icon;

                db.getRolesData(connection).then( (rolesData) => {
                  if (userData.role_id !== 0) {
                    userData.role_title = rolesData[userData.role_id].title;
                    userData.icon = rolesData[userData.role_id].icon;
                  }else{
                    userData.role_title = rolesData[0].title;
                    userData.icon = rolesData[0].icon;
                  }



                  db.getUserRolesInfo(connection, userId).then( (userRolesData) => {
                    let userRolesDataLength = -1;

                    if (userRolesData !== undefined) {
                      userRolesDataLength = userRolesData.length;
                    }

                    db.fetchFavs(connection, userId).then( (favsData) => {
                      let favsCardData = favsData;
                      let i = 0;
                      let favsQuantity = favsData.length;

                      db.getUserNotifs(connection, userId).then( (notifications) => {
                        let notifLength = notifications.length;
                        for (k = 0; k < notifLength; k++) {
                            notifications[k].created = forum.parseDate(notifications[k].created);
                        }

                        (function favsRecursive(i) {
                          // IF FIRST RECURSIVE LOOP COUNTER HAS REACHED LIMIT
                          // THEN
                          if (i === favsQuantity) {

                            db.fetchSubs(connection, userId).then( (subsData) => {
                              let subsCardData = subsData;

                              i = 0;
                              let subsQuantity = subsData.length;
                              (function subsRecursive(i) {
                                // IF FIRST RECURSIVE LOOP COUNTER HAS REACHED LIMIT
                                // THEN
                                if (i === subsQuantity) {
                                  return res.render('profile', {
                                                            title: 'Votre profil',
                                                            userData,
                                                            currentUser,
                                                            favsCardData,
                                                            favsQuantity,
                                                            subsCardData,
                                                            subsQuantity,
                                                            notifications,
                                                            notifLength,
                                                            rolesData,
                                                            userRolesData,
                                                            userRolesDataLength
                                                        });
                                }
                                  db.getInfo(connection, "threads", subsCardData[i].thread_id).then( (threadData) => {
                                    subsCardData[i].title = threadData.title;
                                    subsCardData[i].is_deleted = threadData.is_deleted;
                                    db.checkFavorite(connection, subsCardData[i].thread_id, userId).then( (favResults) => {
                                      if (favResults !== undefined) {
                                        subsCardData[i].isFav = favResults.is_active;
                                      }else {
                                        subsCardData[i].isFav = 0;
                                      }
                                      db.getThreadScoreData(connection, subsCardData[i].thread_id).then( (threadScoreData) => {
                                          hasVotedThread = false;
                                          subsCardData[i].score = 0;
                                          for (j = 0; j < threadScoreData.length; j++) {
                                              subsCardData[i].score += threadScoreData[j].vote;
                                              hasVotedThread = (threadScoreData[j].user_id === userId)?true:false;
                                              if (hasVotedThread) {
                                                  if (threadScoreData[j].vote === 1) {
                                                      subsCardData[i].voteTypeThread = "upvote";
                                                  }else if (threadScoreData[j].vote === 0) {
                                                      subsCardData[i].voteTypeThread = null;
                                                  }else {
                                                  subsCardData[i].voteTypeThread = "downvote";
                                                }
                                              }
                                          }
                                          db.getVoteCount(connection, subsCardData[i].thread_id).then( (voteCount) => {
                                              subsCardData[i].voteCount = voteCount;

                                              db.getAnswerCount(connection, subsCardData[i].thread_id).then( (answerCount) => {
                                                  subsCardData[i].answerCount = answerCount;
                                                  // ITERATE FIRST LOOP IN THE QUERY CALLBACK FUNCTION
                                                  subsRecursive(i+1);
                                              }).catch( (err) => setImmediate( () => { throw err; })); // END OF getAnswerCount QUERY
                                          }).catch( (err) => setImmediate( () => { throw err; })); // END OF getVoteCount QUERY
                                      }).catch( (err) => setImmediate( () => { throw err; })); // END OF getThreadScore QUERY
                                    }).catch( (err) => setImmediate( () => { throw err; })); // END OF checkSub PROMISE
                                  }).catch( (err) => setImmediate( () => { throw err; })); // END OF checkFavorite PROMISE
                              })(0); // END OF RECURSIVE LOOP
                            }).catch( (err) => setImmediate( () => { throw err; })); // END OF fetchSubs PROMISE
                            return;
                          }
                            db.getInfo(connection, "threads", favsCardData[i].thread_id).then( (threadData) => {
                              favsCardData[i].title = threadData.title;
                              favsCardData[i].is_deleted = threadData.is_deleted
                              db.checkSub(connection, favsCardData[i].thread_id, userId).then( (subResults) => {
                                if (subResults !== undefined) {
                                  favsCardData[i].isSub = subResults.is_active;
                                }else {
                                  favsCardData[i].isSub = 0;
                                }
                                db.getThreadScoreData(connection, favsCardData[i].thread_id).then( (threadScoreData) => {
                                    hasVotedThread = false;
                                    favsCardData[i].score = 0;
                                    for (j = 0; j < threadScoreData.length; j++) {
                                        favsCardData[i].score += threadScoreData[j].vote;
                                        hasVotedThread = (threadScoreData[j].user_id === userId)?true:false;
                                        if (hasVotedThread) {
                                            if (threadScoreData[j].vote === 1) {
                                                favsCardData[i].voteTypeThread = "upvote";
                                            }else if (threadScoreData[j].vote === 0) {
                                                favsCardData[i].voteTypeThread = null;
                                            }else {
                                            favsCardData[i].voteTypeThread = "downvote";
                                          }
                                        }
                                    }
                                    db.getVoteCount(connection, favsCardData[i].thread_id).then( (voteCount) => {
                                        favsCardData[i].voteCount = voteCount;

                                        db.getAnswerCount(connection, favsCardData[i].thread_id).then( (answerCount) => {
                                            favsCardData[i].answerCount = answerCount;

                                            // ITERATE FIRST LOOP IN THE QUERY CALLBACK FUNCTION
                                            favsRecursive(i+1);
                                        }).catch( (err) => setImmediate( () => { throw err; })); // END OF getAnswerCount QUERY
                                    }).catch( (err) => setImmediate( () => { throw err; })); // END OF getVoteCount QUERY
                                }).catch( (err) => setImmediate( () => { throw err; })); // END OF getThreadScore QUERY
                              }).catch( (err) => setImmediate( () => { throw err; })); // END OF checkSub PROMISE
                            }).catch( (err) => setImmediate( () => { throw err; })); // END OF checkFavorite PROMISE
                          })(0); // END OF RECURSIVE LOOP
                        }).catch( (err) => setImmediate( () => { throw err; })); // END OF getUserNotifs PROMISE
                      }).catch( (err) => setImmediate( () => { throw err; })); // END OF fetchFavs PROMISE
                  }).catch( (err) => setImmediate( () => { throw err; })); // END OF getUserRolesInfo QUERY
                }).catch( () => setImmediate( () => { throw err; })); // END OF getRolesData PROMISE
              }).catch( (err) => setImmediate( () => { throw err; })); // END OF getInfo messages PROMISE
            }).catch( (err) => setImmediate( () => { throw err; })); // END OF getUserId PROMISE
        }).catch( (err) => setImmediate( () => { throw err; })); // END OF getUserId PROMISE
});

module.exports = router;
