const express = require('express');
const router = express.Router();

const fs = require('fs');
const crypto = require('crypto');

const db = require('../modules/db');
const userManagement = require('../modules/userManagement');
const connection = db.connect();
const config = JSON.parse(fs.readFileSync(__dirname+'/../config/config.json'));

router.get('/', (req, res, next) => {
    let currentUser = userManagement.getUsername(req);

    if ( !(userManagement.authenticate(req, res, {user : "_admin"})) ){
      res.redirect('/');
      return;
    }

    // DISPLAY ALL THE CLASSES
    let classeData = [];

    connection.query("SELECT * FROM users", (err, usersData, fields) => {
        let usersDataLength = usersData.length;
        db.getUserId(connection, currentUser).then( (userId) => {
          db.getUserNotifs(connection, userId).then( (notifications) => {
            let notifLength = notifications.length;
            for (k = 0; k < notifLength; k++) {
                notifications[k].created = forum.parseDate(notifications[k].created);
            }

            db.getClasses(connection).then( (results) => {
                classesData = results;

                res.render('admin', {
                                       title: config.title,
                                       usersData,
                                       usersDataLength,
                                       classesData,
                                       currentUser,
                                       notifications,
                                       notifLength
                                     });
            });
          }).catch( (err) => setImmediate( () => { throw err; })); // END OF getUserNotifs PROMISE
        }).catch( (err) => setImmediate( () => { throw err; })); // END OF getUserId PROMISE
    });

});// END OF router /admin

module.exports = router;
