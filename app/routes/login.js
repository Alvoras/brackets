const express = require('express');
const router = express.Router();

const fs = require('fs');
const crypto = require('crypto');
const bcrypt = require('bcryptjs');
const userManagement = require('../modules/userManagement');
const db = require('../modules/db');
const connection = db.connect();
const bodyParser = require('body-parser');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const config = JSON.parse(fs.readFileSync(__dirname+'/../config/config.json'));
const cookieConfig = JSON.parse(fs.readFileSync(__dirname+'/../config/cookie.json'));

router.use(cookieParser());

router.use(session({
  "secret": config.secrets.sessionSecret,
  "expires": new Date(Date.now() + 7*24*60*60*1000), // ONE WEEK
  "resave": cookieConfig.resave,
  "saveUninitialized" : cookieConfig.saveUninitialized,
  "cookie": {
    "secure": cookieConfig.cookie.secure,
    "expires" : new Date(Date.now() + 7*24*60*60*1000)
  }
}));


router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.get('/', (req, res, next) => {

  let login = false;
  if (req.session.error) {
    var error = req.session.error;
  }

  if (req.session.user){
    delete req.session.user;
  }

  if (req.cookies["rememberMe"]) {
    let [username, token, hmac] = req.cookies["rememberMe"].split(':');
    let cookie = [];
    cookie["username"] = username;
    cookie["token"] = token;
    cookie["hmac"] = hmac;

    db.getUserToken(connection, cookie["username"]).then( (dbToken) => {
        let hmac = crypto.createHmac('sha256', config.secrets.hmacSecret);
        hmac.update(cookie["username"]+':'+cookie["token"])
        let localHmac = hmac.digest('hex');

        hmac = '';
        hmac = crypto.createHmac('sha256', config.secrets.hmacSecret);
        hmac.update(cookie["username"]+':'+dbToken);
        let dbHmac = hmac.digest('hex');

        if ( !(userManagement.checkHmac(localHmac, dbHmac)) ){
          // delete cookie and return
          res.cookie("rememberMe", '');
          res.render('login', {title: "Login", error});
          console.log("Bad HMAC");
        }else {
          if ( !(userManagement.checkToken(cookie["token"], dbToken)) ){
            // delete cookie and return
            res.cookie("rememberMe", '');
            res.render('login', {title: "Login", error});
            console.log("Bad token");
          }else {
            // Log in
            login = true;
            req.session.user = cookie["username"];
          }
        }

        if ( (login) ){
          // DISPLAY WELCOME BACK
          res.redirect('/front/all/0');
        }else {
          res.render('login', {title: "Login", error});
        }
    }).catch( (err) => setImmediate( () => { throw err; })); // END OF getUserToken PROMISE
  }else {
    res.render('login', {title: "Login", error});
  }
});

router.post('/login', (req, res, next) => {
  let username = req.body.loginUsername;
  let pwd = req.body.loginPassword;
  let errors = [];

  if (username === undefined || pwd === undefined || username === ""  || pwd === "") {
    errors[0] = true;
    req.session.error = "";
    console.log("HTML HAS BEEN MODIFIED");
  }else {

    // BINARY keyword makes the query case sensitive
    let getPwdQuery = "SELECT password FROM users WHERE pseudo = BINARY ?";

    connection.query(getPwdQuery, username, (err, results, fields) => {
      if (results[0] === undefined) {
        errors[2] = true;
        req.session.error = "Identifiant et/ou mot de passe incorrect";
        res.redirect('/');
        return;
      }
      let dbPwd = results[0].password;

      bcrypt.compare(pwd, dbPwd, (err, match) => {
        if ( (match) ) {
          if (req.session.error){
            delete req.session.error;
          }

          // GenerateToken()
          // Push token into db
          // cookie = user:token
          // mac = hash(sha256, cookie)
          // cookie += :mac
          // setCookie("rememberme", cookie)

          let token = userManagement.generateToken(112); // GENERATE 224 bytes string

          db.pushToken(connection, token, username).then( (err) => {
            if (err) throw err;

            let cookie = username+':'+token;
            let hmac = crypto.createHmac('sha256', config.secrets.hmacSecret);
            hmac.update(cookie)
            cookie += ':'+hmac.digest('hex');

            res.cookie('rememberMe', cookie, {
              expires: new Date(Date.now() + 7*24*60*60*1000), // ONE WEEK
              httpOnly: false
            });

            db.getUserId(connection, username).then( (userId) => {
              req.session.user = username;
              req.session.userId = userId;
              req.session.save( (err) => {
                res.redirect('/front/all/0');
                return;
              });
            }).catch( (err) => setImmediate( () => { throw err; })); // END OF getUserId PROMISE
          }).catch( (err) => setImmediate( () => { throw err; })); // END OF pushToken PROMISE

        }else {
          errors[1] = true;
          req.session.error = "Identifiant et/ou mot de passe incorrect";
          res.redirect('/');
        }
      });
    });
  }

  if (errors[0] === true) {
    console.log("HTML has been modified");
  }else if (errors[1] === true) {

  }

  //res.render('login', {title: "Login", errorList: errors});
});

router.get('/disconnect', (req, res, next) => {
  res.cookie("rememberMe", '');
  res.redirect('/');
});

module.exports = router;
