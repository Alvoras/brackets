const express = require('express');
const router = express.Router();

const bodyParser = require('body-parser');
const fs = require('fs');

const forum = require('../modules/forum');
const db = require('../modules/db');
const userManagement = require('../modules/userManagement');
const connection = db.connect();
const config = JSON.parse(fs.readFileSync(__dirname + '/../config/config.json'));
const categories = JSON.parse(fs.readFileSync(__dirname + '/../config/categories.json'));

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: false
}));

router.get('/', (req, res, next) => {
    let currentUser = userManagement.getUsername(req);

    if (!(userManagement.authenticate(req, res, {
            user: currentUser
        }))) {
        res.redirect('/');
        return;
    }

    db.getUserId(connection, currentUser).then((userId) => {
        db.getUserNotifs(connection, userId).then((notifications) => {
            let notifLength = notifications.length;
            for (k = 0; k < notifLength; k++) {
                notifications[k].created = forum.parseDate(notifications[k].created);
            }

            res.render('home-categories', {
                title: config.title,
                categories,
                currentUser,
                notifications,
                notifLength
            });

        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getUserNotifs PROMISE
    }).catch((err) => setImmediate(() => {
        throw err;
    })); // END OF getUserId PROMISE
});

router.get('/:category/:sortMode/:page', (req, res, next) => {
    let currentUser = userManagement.getUsername(req);

    if ( !(userManagement.authenticate(req, res, {user : currentUser})) ){
      res.redirect('/');
      return;
    }

    forum.loadThreadWall(req, res);

    /*let sortMode = req.params.sortMode;

    let sortModeDisplay = "Plus récent"; // default

    switch (sortMode) {
        case "best":
            sortModeDisplay = "Meilleur";
            break;
        case "vote":
            sortModeDisplay = "Plus de vote";
            break;
        default:

    }
    let currentUser = userManagement.getUsername(req);

    let page = req.params.page;

    if (!(userManagement.authenticate(req, res, {
            user: currentUser
        }))) {
        res.redirect('/');
        return;
    }

    let category = req.params.category;

    db.getThreadsCount(connection).then((threadCount) => {
        let maxPage = threadCount / 20;

        db.getUserInfo(connection, currentUser).then((userData) => {
            let isMod = userData.isMod;
            db.getCategoryThreads(connection, category, sortMode, page).then((results) => {
                let i = 0;
                let data = results;
                let dataLength = data.length;
                let noResults = (dataLength > 0) ? false : true;

                console.log("data", data);

                db.getUserNotifs(connection, userData.id).then((notifications) => {
                    let notifLength = notifications.length;
                    for (k = 0; k < notifLength; k++) {
                        notifications[k].created = forum.parseDate(notifications[k].created);
                    }

                    res.render('index', {
                        title: category,
                        dataLength,
                        data,
                        categories,
                        currentUser,
                        isMod,
                        notifications,
                        notifLength,
                        noResults,
                        sortModeDisplay,
                        sortMode,
                        maxPage,
                        page,
                        userData
                    });
                }).catch((err) => setImmediate(() => {
                    throw err;
                })); // END OF getUserNotifs PROMISE
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF getCategoryThreads PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getUserInfo PROMISE
    }).catch((err) => setImmediate(() => {
        throw err;
    })); // END OF getThreadsCount PROMISE*/
}); // END OF categories

module.exports = router;
