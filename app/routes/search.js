const express = require('express');
const router = express.Router();

const fs = require('fs');
const db = require('../modules/db');
const forum = require('../modules/forum');
const userManagement = require('../modules/userManagement');
const connection = db.connect();
const cookieParser = require('cookie-parser');
const config = JSON.parse(fs.readFileSync(__dirname+'/../config/config.json'));
const categories = JSON.parse(fs.readFileSync(__dirname+'/../config/categories.json'));

router.use(cookieParser());

router.get('/:query/:page', (req, res, next) => {
  let currentUser = userManagement.getUsername(req);
  let searchTerms = req.params.query;

  if ( !(userManagement.authenticate(req, res, {user : currentUser})) ){
    res.redirect('/');
    return;
  }

  let page = 0; // front page

  forum.loadThreadWallBySearch(req, res, page, searchTerms);
});

module.exports = router;
