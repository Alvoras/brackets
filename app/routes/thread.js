const express = require('express');
const router = express.Router();

const forum = require('../modules/forum');
const userManagement = require('../modules/userManagement');
const db = require('../modules/db');
const connection = db.connect();

const fs = require('fs');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const config = JSON.parse(fs.readFileSync(__dirname+'/../config/config.json'));

router.use(cookieParser());

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.get('/:threadId/:page', (req, res, next) => {
    let threadId = req.params.threadId;
    let currentUser = userManagement.getUsername(req);

    if ( !(userManagement.authenticate(req, res, {user : currentUser})) ){
      res.redirect('/');
      return;
    }

    if ( (isNaN(threadId) || threadId.length === 0) ) {
        res.redirect('/');
        return;
    }

    forum.loadThreadContent2(req, res, threadId, currentUser);
});// END OF loadThreadContent

module.exports = router;
