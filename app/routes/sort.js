// UNUSED

const express = require('express');
const router = express.Router();

const fs = require('fs');
const db = require('../modules/db');
const forum = require('../modules/forum');
const userManagement = require('../modules/userManagement');
const connection = db.connect();
const session = require('express-session');
const cookieParser = require('cookie-parser');
const config = JSON.parse(fs.readFileSync(__dirname+'/../config/config.json'));
const categories = JSON.parse(fs.readFileSync(__dirname+'/../config/categories.json'));

router.use(cookieParser());

router.get('/:sortMode/:page', (req, res, next) => {
  let currentUser = userManagement.getUsername(req);

  if ( !(userManagement.authenticate(req, res, {user : currentUser})) ){
    res.redirect('/');
    return;
  }

  forum.loadThreadWall(req, res);
});

module.exports = router;
