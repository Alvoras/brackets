//module.exports = () => {
// server setup
require('console-stamp')(console, '[HH:MM:ss.l]');
const express = require('express');
const app = express();

// Set the environment variable to production, in order to remove all the debug messages displayed
app.set("env", "production");
app.set("useHttps", false);

// modules
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const fs = require('fs');
const bcrypt = require('bcryptjs');
const db = require(__dirname + '/modules/db.js');
const mysql = require('mysql');
const ioCookieParser = require('socket.io-cookie-parser');
const forceSsl = require('express-force-ssl');
const userManagement = require(__dirname + '/modules/userManagement.js');

if (app.get('useHttps')) {
    // Only the default location, adjust the paths accordingly to your architecture
    var key = fs.readFileSync('/etc/letsencrypt/live/esgi-brackets.fr/privkey.pem');
    var cert = fs.readFileSync('/etc/letsencrypt/live/esgi-brackets.fr/cert.pem');
    var creds = {
        key: key,
        cert: cert
    }

    const httpsServer = require('https').createServer(creds, app);
}
const httpServer = require('http').Server(app);

// routes
const index = require('./routes/login');
const front = require('./routes/front');
const sort = require('./routes/sort');
const profile = require('./routes/profile');
const search = require('./routes/search');
const thread = require('./routes/thread');
const categories = require('./routes/categories');
const admin = require('./routes/admin');

// config
const config = JSON.parse(fs.readFileSync(__dirname + '/config/config.json'));
const categoriesList = JSON.parse(fs.readFileSync(__dirname + '/config/categories.json'));
const cookieConfig = JSON.parse(fs.readFileSync(__dirname + '/config/cookie.json'));

// initialization
const connection = db.connect();

if (app.get('useHttps')) {
    const io = require('socket.io')(httpsServer);
}

const io = require('socket.io')(httpServer);

// session
const sharedsession = require('express-socket.io-session');
const session = require('express-session');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon(path.join(__dirname, 'public', 'img', 'favicon', 'favicon.ico')));
app.use(logger('common'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/thread', thread);
app.use('/', index);
app.use('/front', front);
app.use('/s', search);
app.use('/profile', profile);
app.use('/c', categories);
app.use('/admin', admin);

if (app.get('useHttps')) {
    app.use(forceSsl);
}


app.use(session({
    "secret": config.secrets.sessionSecret,
    "expires": new Date(Date.now() + 7 * 24 * 60 * 60 * 1000), // ONE WEEK
    "resave": cookieConfig.resave,
    "saveUninitialized": cookieConfig.saveUninitialized,
    "cookie": {
        "secure": cookieConfig.cookie.secure,
        "expires": new Date(Date.now() + 7 * 24 * 60 * 60 * 1000)
    }
}));

io.use(ioCookieParser());

io.on('connection', (socket) => {
    let username = "";

    if ((socket.request.cookies["rememberMe"] !== undefined)) {
        username = socket.request.cookies["rememberMe"].split(':')[0];
    } else {
        //socket.emit('redirect', '/');
    }

    socket.on('sendReply', (tid, postId, threadTitle, threadOwner, text) => {
        // Get author information
        db.getUserInfo(connection, username).then((userData) => {
            // If the author account is either banned or deleted, cancel post
            if ((userData.is_banned)) {
                console.log("Banned accounts can't post new thread nor reply");
                return;
            } else if ((userData.is_deleted)) {
                console.log("Deleted accounts can't post new thread nor reply");
                return;
            }

            // Get the role additional info to fill the identity card
            db.getInfo(connection, "roles", userData.role_id).then((roleInfo) => {
                let table = "messages";
                let values = {
                    thread_id: tid,
                    text: text,
                    author_id: userData.id
                }
                db.getClass(connection, userData.id).then((classInfo) => {
                    userData.className = classInfo.name;
                    userData.classIcon = classInfo.icon;

                    db.insertInto(connection, table, values).then((results) => {
                        userData.role_title = roleInfo.title;
                        userData.icon = roleInfo.icon;


                        let isBroadcast = true;
                        // Broadcast to update live every clients
                        socket.broadcast.emit('newReply', tid, results.insertId, userData, text, isBroadcast, username);

                        isBroadcast = false;

                        // Broadcast emit to all *other* clients
                        // Re-emit to update current client
                        socket.emit('newReply', tid, results.insertId, userData, text, isBroadcast);

                        // Post-reply analyze
                        // Mentions
                        if (text.indexOf('@') !== -1) {
                            let type = "mention";
                            let memberRgx = /@(\w*)/;
                            let member = text.match(memberRgx)[1];

                            let data = {
                                threadTitle,
                                memberName: member,
                                threadId: tid,
                                postId,
                                username
                            }

                            db.addNotification(connection, socket, type, data);
                        }

                        // look into subscribed table with this thread id
                        //  if userId/threadId found, then add notif to notifications table and send notif to users id
                        // BUG No notifications in chrome 60.0.3112.90 ?

                        // Subscribers notifications
                        db.getThreadSubscribers(connection, tid).then((subscribers) => {
                            let i = 0;
                            let type = "subscribe";
                            let data = {
                                threadTitle,
                                threadId: tid,
                                username
                            };
                            (function sendNotificationRecursive(i) {
                                if (i === subscribers.length) {
                                    // End recursive loop
                                    return;
                                }

                                data.subscriberId = subscribers[i].user_id;

                                if (userData.id !== subscribers[i].user_id) {
                                    db.addNotification(connection, socket, type, data);
                                    sendNotificationRecursive(i + 1);
                                }
                            })(0);
                        }).catch((err) => setImmediate(() => {
                            throw err;
                        })); // END OF getThreadSubscribers PROMISE

                        // look into threads table
                        //  send notif to author_id

                        // New reply notification for threads owners
                        db.getInfo(connection, "threads", tid).then((threadData) => {
                            if (userData.id !== threadData.author_id) {
                                let type = "newAnswer";
                                let data = {
                                    threadTitle,
                                    threadId: tid,
                                    postId,
                                    username,
                                    author_id: threadData.author_id
                                }

                                db.addNotification(connection, socket, type, data);
                            }
                        }).catch((err) => setImmediate(() => {
                            throw err;
                        })); // END OF getInfo PROMISE

                        // Get existing roles of the replying user
                        // If last reply crossed a new distinction threshold, add it
                        // to the list and display notification

                        // Check distinctions unlock
                        db.getUserMessageCount(connection, userData.id).then((messageCount) => {
                            db.getRolesData(connection, userData.id).then((roleData) => {
                                let i = 0;
                                let messageDistinctionsThreshold = [1, 10, 25, 50, 100, 250, 500];

                                for (i = messageDistinctionsThreshold.length - 1; i >= 0; i--) {
                                    // Find the highest threshold crossed
                                    if (messageCount >= messageDistinctionsThreshold[i]) {
                                        let roleId = (i + 1) + 3; // i finish at 0 while roles id start at 1, and there is 3 more role before the messages' roles

                                        // If this threshold has NOT already been registered in db
                                        db.checkRole(connection, userData.id, roleId).then((hasRole) => {
                                            if (!(hasRole)) {
                                                // Add it to db
                                                let values = {
                                                    user_id: userData.id,
                                                    role_id: roleId
                                                }

                                                db.addNewUserRole(connection, values).then(() => {
                                                    // Display unlock notification
                                                    let type = "messageRoleUnlocked";
                                                    let data = {
                                                        user_id: userData.id,
                                                        username,
                                                        role_name: roleData[roleId].title
                                                    };
                                                    db.addNotification(connection, socket, type, data);

                                                }).catch(() => setImmediate(() => {
                                                    throw err;
                                                })); // END OF addNewUserRole PROMISE
                                            }
                                        }).catch(() => setImmediate(() => {
                                            throw err;
                                        })); // END OF checkRole PROMISE
                                        // Disable the break to allow retro-active role unlocking
                                        // -> a user without any records in users_roles and >50 message will unlock roles 4, 5, 6, 7 all at once
                                        // Since there should not never be the need for it (all users start with a message count at 0), we disable it by default
                                        break;
                                    }
                                }



                            }).catch((err) => setImmediate(() => {
                                throw err;
                            })); // END OF getRolesData PROMISE
                        }).catch((err) => setImmediate(() => {
                            throw err;
                        })); // END OF getUserMessageCount PROMISE
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF insertInto PROMISE
                }).catch((err) => setImmediate(() => {
                    throw err;
                })); // END OF getClass PROMISE
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF getInfo users PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getInfo roles PROMISE
    }); // END OF ON SENDREPLY

    socket.on('openNewThread', (threadData) => {
        let htmlVerified = false;

        let verifyHtml = (array) => {
            for (let i = 0; i < array.length; i++) {
                if (array[i] === threadData.category) {
                    return true;
                }
            }
        }

        if (!(verifyHtml(categoriesList.languages))) {
            console.log("FAIL LANGUAGES HTML FALSIFIED");
            if (!(verifyHtml(categoriesList.frameworks))) {
                console.log("FAIL FRAMEWORKS HTML FALSIFIED");
                if (!(verifyHtml(categoriesList.subjects))) {
                    console.log("FAIL CATEGORIES HTML FALSIFIED");
                    socket.emit('redirect', '/disconnect');
                    return;
                }
            }
        }

        db.getUserInfo(connection, username).then((userData) => {
            if (threadData === undefined) {
                return;
            }
            threadData.author_id = userData.id;
            let isMod = userData.is_mod;

            if ((userData.is_banned) || (userData.is_deleted)) {
                // redirect to disconnect page to handle cookies deletion
                console.log("Banned accounts can't post new thread nor reply");
                socket.emit('redirect', '/disconnect');
                return;
            }

            db.getLastThreadId(connection).then((lastThreadId) => {
                let table = "threads";

                threadData.last_modified = Date.now();

                db.insertInto(connection, table, threadData).then(() => {
                    if (lastThreadId === undefined) {
                        threadData.id = 1;
                    } else {
                        threadData.id = lastThreadId.id + 1;
                    }

                    let isSender = false;

                    // Broadcast to live update every clients
                    socket.broadcast.emit('newThreadOpened', threadData, isMod, isSender);

                    isSender = true;

                    // Broadcast emit to all *other* clients
                    // Re-emit to update current client
                    socket.emit('newThreadOpened', threadData, isMod, isSender);
                }).catch((err) => setImmediate(() => {
                    throw err;
                })); // END OF insertInto PROMISE
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF GET lastThreadId PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getUserInfo PROMISE
    }); // END OF ON openNewThread

    socket.on('addNewUser', (values) => {
        let plainPassword = userManagement.generatePwd(12);

        let salt = bcrypt.genSaltSync(10);
        let encryptedPwd = bcrypt.hashSync(plainPassword, salt);

        values.password = encryptedPwd;

        connection.query("SELECT id FROM users WHERE email = ?", values.email, (err, results, fields) => {
            if (results.length > 0) {
                // email already in use
                socket.emit('lightNotif', "Un utilisateur possède déjà cet email");
                return;
            } else {
                connection.query("SELECT id FROM users WHERE pseudo = ?", values.pseudo, (err, results, fields) => {
                    if (results.length > 0) {
                        // pseudo already in use
                        socket.emit('lightNotif', "Un utilisateur possède déjà ce pseudo");
                        return;
                    } else {
                        let mailUserData = {
                            username: values.pseudo,
                            password: plainPassword
                        };

                        // Send mail with temporary credentials
                        userManagement.sendWelcomeMail(values.email, mailUserData).then(() => {
                            // If the mail is correctly sent, add user to DB
                            db.insertInto(connection, "users", values).then(() => {
                                socket.emit('userAdded');
                            }).catch((err) => setImmediate(() => {
                                throw err;
                            })); // END OF insertInto PROMISE
                        });
                    }
                });
            }
        });
    }); // END OF addNewUser

    socket.on('validateMessage', (tid, postId, chosenName) => {
        let values = {
            is_chosen: 1
        };

        db.updateMessage(connection, values, postId).then(() => {
            delete values.is_chosen;
            values = {
                is_validated: 1
            };
            db.updateThread(connection, values, tid).then(() => {
                db.updateScore(connection, chosenName).then(() => {
                    // Broadcast to live update every clients
                    socket.broadcast.emit('messageValidated', tid, postId);

                    // Broadcast emit to all *other* clients
                    // Re-emit to update current client
                    socket.emit('messageValidated', tid, postId);

                    // send notification to the author of the validated reply
                    db.getUserId(connection, chosenName).then((chosenUserId) => {
                        db.getInfo(connection, "threads", tid).then((threadData) => {
                            let type = "validateAnswer";
                            let data = {
                                threadTitle: threadData.title,
                                threadId: tid,
                                postId: postId,
                                username: username,
                                chosenId: chosenUserId,

                            }

                            db.addNotification(connection, socket, type, data);
                        }).catch((err) => setImmediate(() => {
                            throw err;
                        })); // END OF getInfo threads PROMISE

                        db.getUserScore(connection, chosenUserId).then((userScore) => {
                            db.getRolesData(connection, chosenUserId).then((roleData) => {
                                let i = 0;
                                let scoreDistinctionsThreshold = [20, 100, 300, 600, 1000, 2000, 3000];

                                for (i = scoreDistinctionsThreshold.length - 1; i >= 0; i--) {
                                    // Find the highest threshold crossed
                                    if (userScore >= scoreDistinctionsThreshold[i]) {
                                        let roleId = (i + 1) + 10; // i finish at 0 while roles id start at 1, and there is 10 more role before the score's roles

                                        // If this threshold has NOT already been registered in db
                                        db.checkRole(connection, chosenUserId, roleId).then((hasRole) => {
                                            if (!(hasRole)) {
                                                // Add it to db
                                                let values = {
                                                    user_id: chosenUserId,
                                                    role_id: roleId
                                                }

                                                db.addNewUserRole(connection, values).then(() => {
                                                    // Display unlock notification
                                                    let type = "scoreRoleUnlocked";
                                                    let data = {
                                                        user_id: chosenUserId,
                                                        username: chosenName,
                                                        role_name: roleData[roleId].title
                                                    };
                                                    db.addNotification(connection, socket, type, data);

                                                }).catch(() => setImmediate(() => {
                                                    throw err;
                                                })); // END OF addNewUserRole PROMISE
                                            }
                                        }).catch(() => setImmediate(() => {
                                            throw err;
                                        })); // END OF checkRole PROMISE
                                        // Disable the break to allow retro-active role unlocking
                                        // (see message's role unlock comment)
                                        break;
                                    }
                                }
                            }).catch((err) => setImmediate(() => {
                                throw err;
                            })); // END OF getRolesData PROMISE
                        }).catch((err) => setImmediate(() => {
                            throw err;
                        })); // END OF getUserMessageCount PROMISE
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF getUserId PROMISE
                }).catch((err) => setImmediate(() => {
                    throw err;
                })); // END OF updateScore PROMISE
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF updateThread PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF updateMessage PROMISE
    }); // END OF validateMessage

    socket.on('updatePost', (postId, text) =>  {
        let currentIsoDate = new Date();

        // Set to paris timezone
        currentIsoDate.setHours(currentIsoDate.getHours()+1);
        //currentIsoDate = currentIsoDate.toISOString().slice(0, 19).replace('T', ' ');

        let values = {
            text: text,
            edited: 1,
            last_modified: currentIsoDate
        };

        db.updateMessage(connection, values, postId).then(() => {
            socket.broadcast.emit('postUpdated', postId, text);

            socket.emit('postUpdated', postId, text);
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF updateMessage PROMISE
    }); // END OF updatePost

    socket.on('addFavorite', (threadId) =>  {
        db.getUserId(connection, username).then((userId) => {
            db.checkFavorite(connection, threadId, userId).then((results) => {
                if (results === undefined) {
                    let values = {
                        thread_id: threadId,
                        user_id: userId
                    };

                    db.insertInto(connection, 'favorited', values).then(() => {
                        socket.emit('favUpdated', threadId);
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF insertInto PROMISE
                } else {
                    db.updateFavorite(connection, threadId, userId).then(() => {
                        socket.emit('favUpdated', threadId); // TODO UPDATE BUILDING BLOCKS
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF insertInto PROMISE
                }
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF checkFavorite PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getUserId PROMISE
    }); // END OF addFavorite

    socket.on('addSubscribe', (threadId) => {
        db.getUserId(connection, username).then((userId) => {
            db.checkSub(connection, threadId, userId).then((results) => {
                if (results === undefined) {
                    let values = {
                        thread_id: threadId,
                        user_id: userId
                    };

                    db.insertInto(connection, 'subscribed', values).then(() => {
                        socket.emit('subUpdated', threadId);
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF insertInto PROMISE
                } else {
                    db.updateSub(connection, threadId, userId).then(() => {
                        socket.emit('subUpdated', threadId);
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF insertInto PROMISE
                }
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF checkFavorite PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getUserId PROMISE
    }); // END OF addSubscribe

    socket.on('voteThread', (threadId, voteMode, hadPreviouslyVoted) => {
        db.getUserId(connection, username).then((userId) => {

            db.hasUserVotedThread(connection, threadId, userId).then((vote) =>  {
                if (vote === undefined) {
                    // Add new record
                    let values = {
                        user_id: userId,
                        thread_id: threadId,
                        vote: (voteMode === "upvote") ? 1 : -1
                    }
                    db.insertInto(connection, "threads_votes", values).then(() =>  {
                        let isBroadcast = true;
                        socket.broadcast.emit('voteThreadDone', threadId, values.vote, isBroadcast, hadPreviouslyVoted);

                        isBroadcast = false;
                        socket.emit('voteThreadDone', threadId, values.vote, isBroadcast, hadPreviouslyVoted);
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF insertInto PROMISE
                } else {
                    // Toggle existing record
                    db.toggleVote(connection, "threads_votes", threadId, userId, voteMode).then(() => {
                        let currentVote = (voteMode === "upvote") ? 1 : -1;
                        let isBroadcast = true;
                        socket.broadcast.emit('voteThreadDone', threadId, currentVote, isBroadcast, hadPreviouslyVoted);

                        isBroadcast = false;
                        socket.emit('voteThreadDone', threadId, currentVote, isBroadcast, hadPreviouslyVoted);
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF toggleVote PROMISE
                }
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF hasUserVoted PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getUserId PROMISE
    }); // END OF voteThread

    socket.on('voteMessage', (messageId, threadId, voteMode, hadPreviouslyVoted) => {
        db.getUserId(connection, username).then((userId) => {
            db.hasUserVotedMessage(connection, messageId, threadId, userId).then((vote) =>  {
                if (vote === undefined) {
                    // Insert into
                    let values = {
                        message_id: messageId,
                        thread_id: threadId,
                        user_id: userId,
                        vote: (voteMode === "upvote") ? 1 : -1
                    }

                    db.insertInto(connection, "messages_votes", values).then(() =>  {
                        let isBroadcast = true;
                        socket.broadcast.emit('voteMessageDone', threadId, messageId, values.vote, isBroadcast, hadPreviouslyVoted);

                        isBroadcast = false;
                        socket.emit('voteMessageDone', threadId, messageId, values.vote, isBroadcast, hadPreviouslyVoted);
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF insertInto PROMISE
                } else {
                    // Toggle
                    db.toggleVote(connection, "messages_votes", threadId, userId, voteMode, messageId).then(() => {
                        let currentVote = (voteMode === "upvote") ? 1 : -1;

                        let isBroadcast = true;
                        socket.broadcast.emit('voteMessageDone', threadId, messageId, currentVote, isBroadcast, hadPreviouslyVoted);

                        isBroadcast = false;
                        socket.emit('voteMessageDone', threadId, messageId, currentVote, isBroadcast, hadPreviouslyVoted);
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF toggleVote PROMISE
                }
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF hasUserVoted PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getUserId PROMISE
    }); // END OF voteMessage

    socket.on('updateFrontThread', (values, threadId) => {
        db.updateThread(connection, values, threadId).then(() => {
            socket.broadcast.emit('frontThreadUpdated', threadId, values);

            socket.emit('frontThreadUpdated', threadId, values);
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF updateThread PROMISE
    }); // END OF updateThread

    socket.on('updateThreadFromMessage', (values, threadId) => {
        db.updateThread(connection, values, threadId).then(() => {
            socket.broadcast.emit('fromMessageThreadUpdated', threadId, values);

            socket.emit('fromMessageThreadUpdated', threadId, values);
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF updateThread PROMISE
    }); // END OF updateThread

    socket.on('loadNextPage', (pageNumber, sortMode) => {
        db.getThreads(connection, sortMode, pageNumber).then((threadData) =>  {
            let i = 0;
            let j = 0;
            let dataLength = threadData.length;
            let data = threadData;
            let threadQuantity = threadData.length;
            let currentUser = username;

            if (threadData.length === 0) {
                // If all the thread has been displayed, cancel
                return;
            }

            db.getUserInfo(connection, currentUser).then((userData) => {
                let userId = userData.id;
                let isMod = userData.is_mod;
                (function dbQueryRecursive(i) {
                    // IF FIRST RECURSIVE LOOP COUNTER HAS REACHED LIMIT
                    // THEN
                    if (i === threadQuantity) {
                        socket.emit('nextPageLoaded', dataLength, data, pageNumber, isMod);

                        return; // NEEDED TO STOP THE LOOP ?
                    }
                    db.checkFavorite(connection, data[i].id, userId).then((favResults) => {
                        if (favResults !== undefined) {
                            data[i].isFav = favResults.is_active;
                        } else {
                            data[i].isFav = 0;
                        }
                        db.checkSub(connection, data[i].id, userId).then((subResults) => {
                            if (subResults !== undefined) {
                                data[i].isSub = subResults.is_active;
                            } else {
                                data[i].isSub = 0;
                            }
                            db.getThreadScoreData(connection, data[i].id).then((threadScoreData) => {
                                hasVotedThread = false;
                                data[i].score = 0;
                                for (j = 0; j < threadScoreData.length; j++) {
                                    data[i].score += threadScoreData[j].vote;
                                    hasVotedThread = (threadScoreData[j].user_id === userId) ? true : false;
                                    if (hasVotedThread) {
                                        if (threadScoreData[j].vote === 1) {
                                            data[i].voteTypeThread = "upvote";
                                        } else if (threadScoreData[j].vote === 0) {
                                            data[i].voteTypeThread = null;
                                        } else {
                                            data[i].voteTypeThread = "downvote";
                                        }
                                    }
                                }
                                db.getVoteCount(connection, data[i].id).then((voteCount) => {
                                    data[i].voteCount = voteCount;

                                    db.getAnswerCount(connection, data[i].id).then((answerCount) => {
                                        data[i].answerCount = answerCount;

                                        // ITERATE FIRST LOOP IN THE QUERY CALLBACK FUNCTION
                                        dbQueryRecursive(i + 1);
                                    }).catch((err) => setImmediate(() => {
                                        throw err;
                                    })); // END OF getAnswerCount QUERY
                                }).catch((err) => setImmediate(() => {
                                    throw err;
                                })); // END OF getVoteCount QUERY
                            }).catch((err) => setImmediate(() => {
                                throw err;
                            })); // END OF getThreadScoreData QUERY
                        }).catch((err) => setImmediate(() => {
                            throw err;
                        })); // END OF checkSub PROMISE
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF checkFavorite PROMISE
                })(0); // END OF RECURSIVE LOOP
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF getUserInfo PROMISE
        })
    });

    socket.on('isNotificationClient', (values) => {
        db.getInfo(connection, "users", values.userId).then((userData) => {

            console.log("values", values);

            if (username === userData.pseudo) {
                socket.emit('receiveNotification', (values));
            }
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getUserInfo PROMISE
    });

    socket.on('markNotificationsAsRead', (notificationIds, removeNotifications) => {
        let i = 0;
        (function markNotificationsAsReadRecursive(i) {
            if (i === notificationIds.length) {
                if ((removeNotifications)) {
                    socket.emit('notificationsHasBeenRead');
                }
                return;
            }

            db.markNotificationAsRead(connection, notificationIds[i]).then(() => {
                markNotificationsAsReadRecursive(i + 1);
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF updateNotification PROMISE

        })(0);
    });

    socket.on('selectRole', (roleId) => {
        db.getUserId(connection, username).then((userId) => {
            db.checkRole(connection, userId, roleId).then((hasRole) => {
                if ((hasRole)) {
                    db.selectRole(connection, roleId, userId).then(() => {
                        // Display notification "Role changed"
                        // Live update role
                        db.getInfo(connection, "roles", roleId).then((roleData) => {
                            socket.emit('roleChanged', roleData);
                        }).catch((err) => setImmediate(() => {
                            throw err;
                        })); // END OF getInfo PROMISE
                    }).catch((err) => setImmediate(() => {
                        throw err;
                    })); // END OF selectRole PROMISE
                } else {
                    socket.emit('redirect', '/disconnect');
                }
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF checkRole PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getUserId PROMISE
    });

    socket.on('adminAction', (params) => {
        if (params.mode === "ban") {
            // newState is the mirror of the current state
            params.newState = (params.currentState === '1') ? 0 : 1;

            connection.query("UPDATE users SET is_banned = ? WHERE id = ?", [params.newState, params.userId], (err, results, fields) => {
                socket.emit('adminActionDone', params);
            });
        } else if (params.mode === "delete") {
            // newState is the mirror of the current state
            params.newState = (params.currentState === '1') ? 0 : 1;

            connection.query("UPDATE users SET is_deleted = ? WHERE id = ?", [params.newState, params.userId], (err, results, fields) => {
                socket.emit('adminActionDone', params);
            });
        } else if (params.mode === "reset") {
            // newState is the mirror of the current state
            params.newState = (params.currentState === '1') ? 0 : 1;

            connection.query("UPDATE users SET first_time = ? WHERE id = ?", [params.newState, params.userId], (err, results, fields) => {
                socket.emit('adminActionDone', params);
            });
        }
    });

    socket.on('definePassword', (formData) => {
        let salt = bcrypt.genSaltSync(10);
        let encryptedPwd = bcrypt.hashSync(formData.password, salt);

        connection.query('UPDATE users SET password = ?, first_time = 0 WHERE token = ?', [encryptedPwd, formData.token], (err, results, fields) => {
            let success = true;

            if (err !== null) {
                success = false;
            }

            socket.emit('definePasswordDone', success);
        });
    });

    socket.on('reportPost', (postId) => {
        let data = {};
        db.getInfo(connection, "messages", postId).then((messagesResults) => {
            data.postId = postId;
            data.postContent = messagesResults.text;
            db.getInfo(connection, "threads", messagesResults.thread_id).then((threadsResults) => {
                data.threadTitle = threadsResults.title;
                db.getInfo(connection, "users", messagesResults.author_id).then((usersResults) => {
                    data.username = usersResults.pseudo;
                    userManagement.sendReportMail(data);
                    socket.emit('reportSent');
                }).catch((err) => setImmediate(() => {
                    throw err;
                })); // END OF getInfo users PROMISE
            }).catch((err) => setImmediate(() => {
                throw err;
            })); // END OF getInfo messages PROMISE
        }).catch((err) => setImmediate(() => {
            throw err;
        })); // END OF getInfo messages PROMISE
    });

    socket.on("sendNewPassword", (userData) => {
        // check if user exists
        //   if not then throw sendNewPasswordFailed
        //   if yes check if email exists
        //      if not then throw sendNewPasswordFailed
        //      if yes then reset user pwd in db and send mail with infos to user

        userManagement.resetId(connection, socket, userData);
    });
}); // END OF IO.CONNECTION

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

httpServer.listen(config.server.http.port, '0.0.0.0') // 80

if (app.get('useHttps')) {
    httpsServer.listen(config.server.https.port, '0.0.0.0') // 443
}
//}
