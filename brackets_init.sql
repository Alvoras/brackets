-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost
-- Généré le :  Sam 27 Janvier 2018 à 20:30
-- Version du serveur :  10.1.26-MariaDB-0+deb9u1
-- Version de PHP :  7.0.27-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `brackets`
--

-- --------------------------------------------------------

--
-- Structure de la table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `icon` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `classes`
--

INSERT INTO `classes` (`id`, `name`, `icon`) VALUES
(0, 'Staff', '/img/classes/staff.png'),
(1, 'ESGI', '/img/classes/direction.png'),
(2, '1i', '/img/classes/1i.png'),
(3, '1a', '/img/classes/1a.png'),
(4, '2i', '/img/classes/2i.png'),
(5, '2a', '/img/classes/2a.png'),
(6, 'AL 1', '/img/classes/al.png'),
(7, 'Big Data 1', '/img/classes/bigdata.png'),
(8, 'Blockchain 1', '/img/classes/blockchain.png'),
(9, 'Web 1', '/img/classes/web.png'),
(10, 'MCSI 1', '/img/classes/mcsi.png'),
(11, 'SRC 1', '/img/classes/src.png'),
(12, '3DJV 1', '/img/classes/3djv.png'),
(13, 'MOC 1', '/img/classes/moc.png'),
(14, 'SI 1', '/img/classes/si.png'),
(15, 'AL 2', '/img/classes/al.png'),
(16, 'Big Data 2', '/img/classes/bigdata.png'),
(17, 'Blockchain 2', '/img/classes/blockchain.png'),
(18, 'Web 2', '/img/classes/web.png'),
(19, 'MCSI 2', '/img/classes/mcsi.png'),
(20, 'SRC 2', '/img/classes/src.png'),
(21, '3DJV 2', '/img/classes/3djv.png'),
(22, 'MOC 2', '/img/classes/moc.png'),
(23, 'SI 2', '/img/classes/si.png'),
(24, 'AL 3', '/img/classes/al.png'),
(25, 'Big Data 3', '/img/classes/bigdata.png'),
(26, 'Blockchain 3', '/img/classes/blockchain.png'),
(27, 'Web 3', '/img/classes/web.png'),
(28, 'MCSI 3', '/img/classes/mcsi.png'),
(29, 'SRC 3', '/img/classes/src.png'),
(30, '3DJV 3', '/img/classes/3djv.png'),
(31, 'MOC 3', '/img/classes/moc.png'),
(32, 'SI 3', '/img/classes/si.png'),
(33, 'Ancien (AL)', '/img/classes/al.png'),
(34, 'Ancien (Big Data)', '/img/classes/bigdata.png'),
(35, 'Ancien (Blockchain)', '/img/classes/blockchain.png'),
(36, 'Ancien (Web)', '/img/classes/web.png'),
(37, 'Ancien (MCSI)', '/img/classes/mcsi.png'),
(38, 'Ancien (SRC)', '/img/classes/src.png'),
(39, 'Ancien (3DJV)', '/img/classes/3djv.png'),
(40, 'Ancien (MOC)', '/img/classes/moc.png'),
(41, 'Ancien (SI)', '/img/classes/si.png');

-- --------------------------------------------------------

--
-- Structure de la table `favorited`
--

CREATE TABLE `favorited` (
  `id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `author_id` int(11) NOT NULL,
  `upvotes` int(11) NOT NULL DEFAULT '0',
  `downvotes` int(11) NOT NULL DEFAULT '0',
  `edited` int(11) NOT NULL DEFAULT '0',
  `is_chosen` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `messages_votes`
--

CREATE TABLE `messages_votes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `vote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `type` varchar(35) NOT NULL,
  `href` varchar(150) DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `icon` varchar(250) NOT NULL,
  `rarity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `roles`
--

INSERT INTO `roles` (`id`, `title`, `icon`, `rarity`) VALUES
(0, 'Nouveau', '/img/icons/roles/pixel.png', 0),
(1, 'Admin', '/img/icons/roles/digital_rainv3.png', 5),
(2, 'Modérateur', '/img/icons/roles/dredd.png', 4),
(3, 'Professeur', '/img/icons/roles/glasses.png', 4),
(4, 'Pixel sauvage', '/img/icons/roles/pixel1s.png', 0),
(5, 'Pixel averti', '/img/icons/roles/pixel2s.png', 0),
(6, 'Grand pixel', '/img/icons/roles/pixel3s.png', 1),
(7, 'Haut pixel', '/img/icons/roles/pixel4s.png', 1),
(8, 'Pixel caféiné', '/img/icons/roles/pixel_coffee.png', 2),
(9, 'Pixel éclairé', '/img/icons/roles/pixel_eclaire.png', 2),
(10, 'Pixel radieux', '/img/icons/roles/pixel_radieux.png', 3),
(11, 'Hackerman', '/img/icons/roles/hackerman.png', 0),
(12, 'Oncle Ben', '/img/icons/roles/uncleben.png', 0),
(13, 'Obi-Wan Kenobi', '/img/icons/roles/kenobi.png', 1),
(14, 'Prof. Chen', '/img/icons/roles/profchen.png', 1),
(15, 'Prof. Xavier', '/img/icons/roles/profx.png', 2),
(16, 'Gandalf', '/img/icons/roles/gandalf.png', 2),
(17, 'Morpheus', '/img/icons/roles/morpheus.png', 3);

-- --------------------------------------------------------

--
-- Structure de la table `subscribed`
--

CREATE TABLE `subscribed` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `threads`
--

CREATE TABLE `threads` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `category` varchar(35) NOT NULL,
  `author_id` int(11) NOT NULL,
  `edited` tinyint(1) NOT NULL DEFAULT '0',
  `is_validated` tinyint(1) NOT NULL DEFAULT '0',
  `is_duplicated` tinyint(1) NOT NULL DEFAULT '0',
  `is_closed` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `last_modified` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `threads_votes`
--

CREATE TABLE `threads_votes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `vote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(30) NOT NULL,
  `password` char(60) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(75) DEFAULT NULL,
  `surname` varchar(75) DEFAULT NULL,
  `token` char(224) NOT NULL,
  `class_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `score` int(11) NOT NULL DEFAULT '0',
  `is_mod` tinyint(1) NOT NULL DEFAULT '0',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `first_time` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `pseudo`, `password`, `email`, `name`, `surname`, `token`, `class_id`, `role_id`, `score`, `is_mod`, `is_admin`, `is_deleted`, `is_banned`, `created`, `last_modified`, `first_time`) VALUES
(1, '_admin', '$2a$10$Oi1zHfUXfGkOJZ1oO/kSnebm7War1nYElNjCnPdvKTWO./9G1V282', 'esgi.brackets@gmail.com', NULL, NULL, '1de932f6ca5e3a7c9d095bb3e3b50780e20ff2e041eb4e40b9cb7eeaefc0246133ee2f05a990980a596548272eb9e36d97d0734a1b2265b39e627ca146f7bf43c32574fc4c671f244c960858db999d26d5c84041bcceacf8b5a55b7a5fc91e5d17b792da1485a95d592c2bfa42c30d83', 0, 1, 0, 1, 1, 0, 0, '2017-10-02 21:45:23', '2017-11-01 20:25:50', 0);

-- --------------------------------------------------------

--
-- Structure de la table `users_roles`
--

CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `users_roles`
--

INSERT INTO `users_roles` (`id`, `role_id`, `user_id`) VALUES
(1, 1, 1),
(2, 4, 1),
(3, 5, 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `favorited`
--
ALTER TABLE `favorited`
  ADD PRIMARY KEY (`id`),
  ADD KEY `thread_id` (`thread_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `thread_id` (`thread_id`),
  ADD KEY `author_id` (`author_id`);

--
-- Index pour la table `messages_votes`
--
ALTER TABLE `messages_votes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `thread_id` (`thread_id`),
  ADD KEY `message_id` (`message_id`);

--
-- Index pour la table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `subscribed`
--
ALTER TABLE `subscribed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `thread_id` (`thread_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_id` (`author_id`);

--
-- Index pour la table `threads_votes`
--
ALTER TABLE `threads_votes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `thread_id` (`thread_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `pseudo` (`pseudo`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `classe_id` (`class_id`);

--
-- Index pour la table `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT pour la table `favorited`
--
ALTER TABLE `favorited`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `messages_votes`
--
ALTER TABLE `messages_votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `subscribed`
--
ALTER TABLE `subscribed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `threads`
--
ALTER TABLE `threads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `threads_votes`
--
ALTER TABLE `threads_votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `favorited`
--
ALTER TABLE `favorited`
  ADD CONSTRAINT `favorited_ibfk_1` FOREIGN KEY (`thread_id`) REFERENCES `threads` (`id`),
  ADD CONSTRAINT `favorited_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`thread_id`) REFERENCES `threads` (`id`),
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `messages_ibfk_3` FOREIGN KEY (`thread_id`) REFERENCES `threads` (`id`),
  ADD CONSTRAINT `messages_ibfk_4` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `messages_votes`
--
ALTER TABLE `messages_votes`
  ADD CONSTRAINT `messages_votes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `messages_votes_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `messages_votes_ibfk_3` FOREIGN KEY (`thread_id`) REFERENCES `threads` (`id`),
  ADD CONSTRAINT `messages_votes_ibfk_4` FOREIGN KEY (`message_id`) REFERENCES `messages` (`id`);

--
-- Contraintes pour la table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `subscribed`
--
ALTER TABLE `subscribed`
  ADD CONSTRAINT `subscribed_ibfk_1` FOREIGN KEY (`thread_id`) REFERENCES `threads` (`id`),
  ADD CONSTRAINT `subscribed_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `threads`
--
ALTER TABLE `threads`
  ADD CONSTRAINT `threads_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `threads_votes`
--
ALTER TABLE `threads_votes`
  ADD CONSTRAINT `threads_votes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `threads_votes_ibfk_2` FOREIGN KEY (`thread_id`) REFERENCES `threads` (`id`);

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`);

--
-- Contraintes pour la table `users_roles`
--
ALTER TABLE `users_roles`
  ADD CONSTRAINT `users_roles_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `users_roles_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
