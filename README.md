# ESGI Brackets v1.1.0b

Don't forget to run `npm i` to install dependencies.

# Install
Copy and rename the `config_git` folder to `config` and replace the sanitized infos.

Run with `npm start` (using `forever`), `nodemon app.js` (dev mode) or `node app.js`.
